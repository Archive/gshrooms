#include "config.h"

#include <gtk/gtk.h>
#include <gconf/gconf-client.h>

#include <gaim/version.h>
#include <gaim/gtkprefs.h>
#include <gaim/gtkplugin.h>
#include <gaim/gtkpluginpref.h>

#include "gshrooms-gaim-common.h"
#include "gshrooms-gaim-server.h"
#include "gshrooms-gaim-client.h"

enum
{
	PIPELINE_LOW,
	PIPELINE_MED,
	PIPELINE_HIGH
};

enum
{
	SOURCE_SOUNDCARD,
	SOURCE_PLAYER,
	SOURCE_SINE
};

/* Hold these globally */
static GShroomsGaimClient *client;
static GShroomsGaimServer *server;
static GConfClient* gconf;

static gboolean
gshrooms_gaim_init_dbus (GaimPlugin *plugin)
{
	GError *error = NULL;
	
	if (!dbus_g_bus_get (DBUS_BUS_SESSION, &error))
	{
		g_print("Unable to connect to dbus: %s\n", error->message);
		g_error_free (error);
		return FALSE;
	}
	
    return TRUE;
}

static gboolean
gshrooms_gaim_plugin_load (GaimPlugin *plugin)
{
	void *conv_handle = gaim_conversations_get_handle();
	void *blist_handle = gaim_blist_get_handle();
	
	g_print ("Plugin Load\n");
	gconf = gconf_client_get_default ();
	if (gconf == NULL) {
		return FALSE;
	}
	
	if (!gshrooms_gaim_init_dbus (plugin)) {
		return FALSE;
	}
	
	server = g_object_new (GSHROOMS_GAIM_TYPE_SERVER, NULL);
	gaim_signal_connect(conv_handle, "sending-im-msg",
						plugin, GAIM_CALLBACK(gshrooms_gaim_server_sending_im), server);
	gaim_signal_connect(conv_handle, "receiving-im-msg",
						plugin, GAIM_CALLBACK(gshrooms_gaim_server_receiving_im), server);
	gaim_signal_connect(blist_handle, "buddy-signed-off",
						plugin, GAIM_CALLBACK(gshrooms_gaim_server_signoff), server);
	gaim_signal_connect(conv_handle, "deleting-conversation",
						plugin, GAIM_CALLBACK(gshrooms_gaim_server_conv_del), server);
						
	client = g_object_new (GSHROOMS_GAIM_TYPE_CLIENT, NULL);
	gaim_signal_connect(conv_handle, "receiving-im-msg",
						plugin, GAIM_CALLBACK(gshrooms_gaim_client_receiving_im), client);
	gaim_signal_connect(conv_handle, "conversation-created",
						plugin, GAIM_CALLBACK(gshrooms_gaim_client_conv_new), client);
	gaim_signal_connect(conv_handle, "deleting-conversation",
						plugin, GAIM_CALLBACK(gshrooms_gaim_client_conv_del), client);					
	gaim_signal_connect(blist_handle, "buddy-signed-off",
						plugin, GAIM_CALLBACK(gshrooms_gaim_client_signoff), client);
	return TRUE;
}

static gboolean
gshrooms_gaim_plugin_unload (GaimPlugin *plugin)
{
	g_print ("Plugin unload\n");
	g_object_unref (server);
	g_object_unref (client);
	g_object_unref (gconf);
	return TRUE;
}

static void
on_pipeline_changed (GtkWidget *combo, GaimPlugin *plugin)
{
	gconf_client_set_int(gconf,
		"/apps/gshrooms/pipeline",
		gtk_combo_box_get_active (GTK_COMBO_BOX (combo)),
		NULL);
}

static void
on_source_changed (GtkWidget *combo, GaimPlugin *plugin)
{
	gconf_client_set_int(gconf,
		"/apps/gshrooms/source",
		gtk_combo_box_get_active (GTK_COMBO_BOX (combo)),
		NULL);
}
                                     
static GtkWidget*
gshrooms_gaim_plugin_get_pref_frame (GaimPlugin *plugin) {
	GtkWidget *ret, *vbox, *table, *combo, *label;
	GtkCellRenderer *renderer;
	GtkListStore *store;
	GtkTreeIter iter;
	g_print ("Build Pref panel\n");
	/* Outside container */
	ret = gtk_vbox_new(FALSE, 18);
	g_object_set (ret,
		  "border-width", 12,
		  NULL);

	/* Configuration frame */
	vbox = GTK_WIDGET (gaim_gtk_make_frame(ret, _("gShrooms Music Sharing")));
	
	table = gtk_table_new (2, 2, FALSE);
	g_object_set (table,
		"column-spacing", 6,
		"row-spacing", 6, 
		NULL);
                                             
	/* Sound Source Label */
	label = gtk_label_new(_("Sound Source:"));
	gtk_table_attach_defaults (GTK_TABLE (table), label, 0, 1, 0, 1);
	g_object_set (label,
		"xalign", 0.0,
		"x-options", GTK_FILL,
		NULL);
	gtk_widget_show (label);
	
	/* Sound Format Label */
	label = gtk_label_new(_("Sound Format:"));
	gtk_table_attach_defaults (GTK_TABLE (table), label, 0, 1, 1, 2);
	g_object_set (label,
		"xalign", 0.0,
		"x-options", GTK_FILL,
		NULL);
	gtk_widget_show (label);
	
	/* Sound Source Combo */
  	store = gtk_list_store_new (2, G_TYPE_STRING, G_TYPE_INT);
  	/* Note: It is important to keep the order of insertion in sync with the
  	   enumeration order !*/
   	gtk_list_store_append (store, &iter);
    gtk_list_store_set (store, &iter,
    	0, _("Low Bandwidth Ogg/Vorbis"),
    	1, PIPELINE_LOW, -1);
    gtk_list_store_append (store, &iter);
    gtk_list_store_set (store, &iter,
    	0, _("Medium Bandwidth Ogg/Vorbis"),
    	1, PIPELINE_MED, -1);
    gtk_list_store_append (store, &iter);
    gtk_list_store_set (store, &iter,
    	0, _("High Bandwidth Ogg/Vorbis"),
    	1, PIPELINE_HIGH, -1);
    
	combo = gtk_combo_box_new_with_model (GTK_TREE_MODEL (store));
	gtk_table_attach_defaults (GTK_TABLE (table), combo, 1, 2, 0, 1);
	g_object_set (label,
		"y-options", GTK_FILL,
		NULL);
		                                            
	renderer = gtk_cell_renderer_text_new ();
	gtk_cell_layout_pack_start (GTK_CELL_LAYOUT (combo), renderer, TRUE);
	gtk_cell_layout_add_attribute (GTK_CELL_LAYOUT (combo),
		renderer, "text", 0);
	
	gtk_widget_show (combo);
	gtk_combo_box_set_active (GTK_COMBO_BOX (combo),
		gconf_client_get_int (gconf, "/apps/gshrooms/pipeline", NULL));
	g_signal_connect (combo, "changed", G_CALLBACK (on_pipeline_changed), plugin);
	
	/* Sound Format Combo */
	store = gtk_list_store_new (2, G_TYPE_STRING, G_TYPE_INT);
	/* Note: It is important to keep the order of insertion in sync with the
  	   enumeration order !*/
    gtk_list_store_append (store, &iter);
    gtk_list_store_set (store, &iter,
    	0, _("Soundcard Input"),
    	1, SOURCE_SOUNDCARD, -1);
    gtk_list_store_append (store, &iter);
    gtk_list_store_set (store, &iter,
    	0, _("Rhythmbox"),
    	1, SOURCE_PLAYER, -1);
    gtk_list_store_append (store, &iter);
    gtk_list_store_set (store, &iter,
    	0, _("Test Source"),
    	1, SOURCE_SINE, -1);
    
	combo = gtk_combo_box_new_with_model (GTK_TREE_MODEL (store));
	gtk_table_attach_defaults (GTK_TABLE (table), combo, 1, 2, 1, 2);
	g_object_set (label,
		"y-options", GTK_FILL,
		NULL);
	
	renderer = gtk_cell_renderer_text_new ();
	gtk_cell_layout_pack_start (GTK_CELL_LAYOUT (combo), renderer, TRUE);
	gtk_cell_layout_add_attribute (GTK_CELL_LAYOUT (combo),
		renderer, "text", 0);
	
	gtk_widget_show (combo);
	gtk_combo_box_set_active (GTK_COMBO_BOX (combo),
		gconf_client_get_int (gconf, "/apps/gshrooms/source", NULL));
	g_signal_connect (combo, "changed", G_CALLBACK (on_source_changed), plugin);
	
	/* Display the whole thing */
	gtk_box_pack_start_defaults (GTK_BOX (vbox), table);
	gtk_widget_show (table);
	
	gtk_widget_show_all(ret);

	return ret;
}

static GaimGtkPluginUiInfo gshrooms_gaim_plugin_ui_info =
{
	gshrooms_gaim_plugin_get_pref_frame
};

static GaimPluginInfo gshrooms_gaim_info =
{
	GAIM_PLUGIN_MAGIC,
	GAIM_MAJOR_VERSION,
	GAIM_MINOR_VERSION,
	GAIM_PLUGIN_STANDARD,		/**< type           */
	GAIM_GTK_PLUGIN_TYPE,		/**< ui_requirement */
	0,							/**< flags          */
	NULL,						/**< dependencies   */
	GAIM_PRIORITY_DEFAULT,		/**< priority       */

	"gtk-raphael-gshrooms-gaim-plugin",		/**< id             */
	"gShrooms",					/**< name           */
	VERSION,					/**< version        */
	"Share your music with your buddies.",						/**  summary        */
	"Share the music you are currently listening to, using gshrooms.",	/**  description    */
	"Raphael Slinckx <raphael@slinckx.net>",					/**< author         */
	"http://raphael.slinckx.net/",								/**< homepage       */
	gshrooms_gaim_plugin_load,		/**< load           */
	gshrooms_gaim_plugin_unload,		/**< unload         */
	NULL,							/**< destroy        */
	&gshrooms_gaim_plugin_ui_info,	/**< ui_info        */
	NULL,							/**< extra_info     */
	NULL,
	NULL
};

static void
gshrooms_gaim_plugin_init (GaimPlugin *plugin)
{
	g_print ("Plugin Init\n");
	
}

GAIM_INIT_PLUGIN (musicsharing, gshrooms_gaim_plugin_init, gshrooms_gaim_info)
