#include "gshrooms-gaim-server.h"
#include "gshrooms-gaim-common.h"

#include <gaim/cmds.h>
#include <gaim/util.h>

/* DBus method stubs */
#include "gst-nowplaying-client.h"
#include "gshrooms-client.h"

/* The interval to send keepalive to streamer, since it decrements every 30 sec,
   we use a slightly faster keepalive */
#define GSHROOMS_KEEPALIVE_TIMEOUT 20000

#define GSHROOMS_GAIM_SERVER_GET_PRIVATE(o) (G_TYPE_INSTANCE_GET_PRIVATE ((o), GSHROOMS_GAIM_TYPE_SERVER, GShroomsGaimServerPrivate))

typedef struct _GShroomsGaimServerPrivate GShroomsGaimServerPrivate;

struct _GShroomsGaimServerPrivate
{
	gchar *url;
	guint keep_alive_timeout;
	DBusGProxy *nowplaying_proxy;
	DBusGProxy *streamer_proxy;
	GHashTable *buddydata;
	GaimCmdId music_command_id;
	
	/* These cannot contain '|' */
	gchar *artist;
	gchar *album;
	gchar *title;
};

typedef struct
{
	gboolean announce_sent;
	gboolean announce_ack;
	gboolean active;
} BuddyData;

static void gshrooms_gaim_server_init       (GShroomsGaimServer *server);
static void gshrooms_gaim_server_class_init (GShroomsGaimServerClass *klass);
static void gshrooms_gaim_server_finalize 	(GObject *object);

static gchar *gshrooms_gaim_server_get_server_url (GShroomsGaimServer *server);
static void gshrooms_gaim_server_got_server_url (DBusGProxy *proxy,
											gchar * answer,
											GError *error,
											gpointer server);

static void gshrooms_gaim_server_got_getnowplaying (DBusGProxy *proxy,
											GHashTable *metadata,
											GError *error,
											gpointer server);

static void gshrooms_gaim_server_new_media		(DBusGProxy *proxy,
											gpointer server);
static void gshrooms_gaim_server_now_playing 	(DBusGProxy *proxy,
											GHashTable *metadata,
											gpointer server);

static gboolean gshrooms_gaim_server_send_keepalive (GShroomsGaimServerPrivate *priv);

static GaimCmdRet gshrooms_gaim_server_music_command (GaimConversation *conv,
											const gchar *cmd,
											gchar **args,
											gchar **error,
											GShroomsGaimServer *server);
														
G_DEFINE_TYPE (GShroomsGaimServer, gshrooms_gaim_server, G_TYPE_OBJECT);

static void
gshrooms_gaim_server_class_init (GShroomsGaimServerClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);
	
	g_type_class_add_private (klass, sizeof (GShroomsGaimServerPrivate));
	
	object_class->finalize = gshrooms_gaim_server_finalize;
	
	klass->dbus = dbus_g_bus_get (DBUS_BUS_SESSION, NULL);
}

static void
free_buddy_data (BuddyData *data)
{
	g_free (data);
}

static void
gshrooms_gaim_server_init (GShroomsGaimServer *server)
{
	GShroomsGaimServerPrivate *priv = GSHROOMS_GAIM_SERVER_GET_PRIVATE (server);
	GError *error = NULL;
	
	/* Proxy to retreive metadata of current song */
	priv->nowplaying_proxy = dbus_g_proxy_new_for_name (GSHROOMS_GAIM_SERVER_GET_CLASS (server)->dbus,
                                                  "org.gnome.NowPlaying",
                                                  "/org/gnome/NowPlaying",
                                                  "org.gnome.NowPlaying");
                                                  
    /* Connect to the new song and signal notifications */
	dbus_g_proxy_add_signal (priv->nowplaying_proxy, "NowPlaying", DBUS_TYPE_G_STRING_GVALUE_HASHTABLE, G_TYPE_INVALID);
	dbus_g_proxy_add_signal (priv->nowplaying_proxy, "NewMedia", G_TYPE_INVALID);
    dbus_g_proxy_connect_signal (priv->nowplaying_proxy, "NowPlaying", (GCallback) gshrooms_gaim_server_now_playing, server, NULL);
    dbus_g_proxy_connect_signal (priv->nowplaying_proxy, "NewMedia", (GCallback) gshrooms_gaim_server_new_media, server, NULL);
	
	/* Request Now playing song */
	org_gnome_NowPlaying_get_now_playing_async (priv->nowplaying_proxy, gshrooms_gaim_server_got_getnowplaying, server);
	
	/* Create the proxy to stream server */
	priv->streamer_proxy = dbus_g_proxy_new_for_name (GSHROOMS_GAIM_SERVER_GET_CLASS (server)->dbus,
                                                  "org.gnome.gShrooms",
                                                  "/org/gnome/gShrooms",
                                                  "org.gnome.gShrooms");
                                                  
    /* Start & retreive the url of the stream server */
	priv->keep_alive_timeout = g_timeout_add (GSHROOMS_KEEPALIVE_TIMEOUT,
							(GSourceFunc) gshrooms_gaim_server_send_keepalive,
							priv);
	
	/* Store the buddy data in a persistent way */
	priv->buddydata = g_hash_table_new_full (
							g_str_hash,
							g_str_equal,
							g_free,
							(GDestroyNotify) free_buddy_data);
	
	priv->music_command_id =  gaim_cmd_register ("music", "",
		GAIM_CMD_P_PLUGIN, GAIM_CMD_FLAG_IM,
		NULL,
		(GaimCmdFunc) gshrooms_gaim_server_music_command,
		"music:  Announce your music sharing to the buddy",
		server);  
	
	/* Init default values */
	priv->url = NULL;
	priv->artist = NULL;
	priv->album = NULL;
	priv->title = NULL;
}

static void
gshrooms_gaim_server_finalize (GObject *object)
{
	GShroomsGaimServer *server;
	GShroomsGaimServerPrivate *priv;
	
	g_return_if_fail (object != NULL);
	g_return_if_fail (GSHROOMS_GAIM_IS_SERVER (object));

	server = GSHROOMS_GAIM_SERVER (object);
	priv = GSHROOMS_GAIM_SERVER_GET_PRIVATE (server);
	
	/* Tell streamer we have finished with it */
	g_source_remove (priv->keep_alive_timeout);
	org_gnome_gShrooms_ask_shutdown (priv->streamer_proxy, NULL);
	
	dbus_g_proxy_disconnect_signal (priv->nowplaying_proxy, "NowPlaying", (GCallback) gshrooms_gaim_server_now_playing, server);
	dbus_g_proxy_disconnect_signal (priv->nowplaying_proxy, "NewMedia", (GCallback) gshrooms_gaim_server_new_media, server);
	
	g_object_unref (priv->nowplaying_proxy);
	g_object_unref (priv->streamer_proxy);
	
	g_hash_table_destroy (priv->buddydata);
	
	gaim_cmd_unregister (priv->music_command_id);
	
	g_free (priv->url);
	g_free (priv->artist);
	g_free (priv->album);
	g_free (priv->title);

	G_OBJECT_CLASS (gshrooms_gaim_server_parent_class)->finalize (object);
}

static void
gshrooms_gaim_server_got_keepalive_answer (DBusGProxy *proxy,
							GError *error,
							gpointer priv)
{
	if(error != NULL)
	{
		g_print ("Unable to keep alive server: %s\n", error->message);
		g_error_free (error);
	}
}

static gboolean
gshrooms_gaim_server_send_keepalive (GShroomsGaimServerPrivate *priv)
{
	g_print ("KeepAlive\n");
	org_gnome_gShrooms_keep_alive_async (priv->streamer_proxy, gshrooms_gaim_server_got_keepalive_answer, priv);
	return TRUE;
}

static void
gshrooms_gaim_server_send_metadata (GaimConversation *conv,
							GShroomsGaimServerPrivate *priv)
{
	BuddyData *data = (BuddyData *) g_hash_table_lookup (priv->buddydata, gaim_conversation_get_name(conv));
	GaimConnection *connection = gaim_conversation_get_gc (conv);
	gchar *message;
	
	if (data == NULL || !data->announce_sent || !data->announce_ack || !data->active) {
		/* Don't bug people that haven't been introduced */
		return;
	}
	
	message = g_strdup_printf ("%s%s|%s|%s",
				GSHROOMS_GAIM_NOWPLAYING,
				(priv->artist == NULL)? "" : priv->artist,
				(priv->album == NULL) ? "" : priv->album,
				(priv->title == NULL) ? "" : priv->title);
	
	if (connection != NULL)
	{
		serv_send_im(connection, gaim_conversation_get_name(conv), message, 0);
	}
		
	g_free (message);
}

static void
gshrooms_gaim_server_now_playing (DBusGProxy *proxy, GHashTable *metadata, gpointer server)
{
	GShroomsGaimServerPrivate *priv = GSHROOMS_GAIM_SERVER_GET_PRIVATE (server);
	GValue *val;
	g_print ("Got now playing\n");
	
	g_free (priv->artist);
	g_free (priv->album);
	g_free (priv->title);
	
	/* Extract the needed metadata */
	val = g_hash_table_lookup (metadata, "artist");
	priv->artist = (val != NULL) ? g_strdelimit (g_value_dup_string (val), "|", '-') : NULL;
	
	val = g_hash_table_lookup (metadata, "album");
	priv->album =  (val != NULL) ? g_strdelimit (g_value_dup_string (val), "|", '-') : NULL;
	
	val = g_hash_table_lookup (metadata, "title");
	priv->title =  (val != NULL) ? g_strdelimit (g_value_dup_string (val), "|", '-') : NULL;

	/* Send it through gaim */
	g_list_foreach (gaim_get_ims(), (GFunc) gshrooms_gaim_server_send_metadata, priv);
}

static void
gshrooms_gaim_server_new_media (DBusGProxy *proxy, gpointer server)
{
	GShroomsGaimServerPrivate *priv = GSHROOMS_GAIM_SERVER_GET_PRIVATE (server);
	g_print ("Got New Media\n");
	
	/* Reset Metadata */
	g_free (priv->artist); priv->artist = NULL;
	g_free (priv->album);  priv->album = NULL;
	g_free (priv->title);  priv->title = NULL;
	
	/* Send it through gaim */
	g_list_foreach (gaim_get_ims(), (GFunc) gshrooms_gaim_server_send_metadata, priv);
}

static void
gshrooms_gaim_server_got_getnowplaying (DBusGProxy *proxy, GHashTable *metadata, GError *error, gpointer server)
{
	if(error != NULL)
	{
		g_print ("Unable to retreive currently playing song: %s\n", error->message);
		g_error_free (error);
		return;
	}
	
	gshrooms_gaim_server_now_playing (proxy, metadata, server);
}	
static void
gshrooms_gaim_server_got_server_url (DBusGProxy *proxy,
							gchar * answer,
							GError *error,
							gpointer server)
{
	if (error != NULL)
	{
		g_print ("Failed to retreive server url: %s\n", error->message);
		g_error_free (error);
		return;
	}
	
	g_print ("Got server url: '%s'\n", answer);
	/* Check for NULL and empty string */
	if (answer == NULL || *answer == '\0')
	{
		GSHROOMS_GAIM_SERVER_GET_PRIVATE (server)->url = NULL;
	}
	else
	{
		GSHROOMS_GAIM_SERVER_GET_PRIVATE (server)->url = g_strdup (answer);
	}
}

static gchar *
gshrooms_gaim_server_get_server_url (GShroomsGaimServer *server)
{
	GShroomsGaimServerPrivate *priv = GSHROOMS_GAIM_SERVER_GET_PRIVATE (server);
	
	if (priv->url == NULL) {
		/* We didn't get any url yet, trigger a new request */
		g_print ("No known URL, trigger dbus method\n");
		org_gnome_gShrooms_get_url_async (
				priv->streamer_proxy,
				gshrooms_gaim_server_got_server_url,
				server);
	}
	
	return priv->url;
}

static void
gshrooms_gaim_server_send_announce (GaimConnection *conn,
								const gchar *receiver,
								const gchar *url)
{
	gchar *announce = g_strdup_printf (
			/* tranlators: you MUST leave the first %s in place,
			   The text must follow immediately, second %s is the url, 
			   to use for an href link. You MUST leave the third %s in the
			   last position! */
			_("%s<a href='%s'>Listen</a> to the music! %s"),
			GSHROOMS_GAIM_PREFIX,
			url,
			url);
	serv_send_im(conn, receiver, announce, 0);
		
	g_free (announce);
}

void
gshrooms_gaim_server_sending_im (GaimAccount *account,
								const char *receiver,
								char **message,
								GShroomsGaimServer *server)
{
	GShroomsGaimServerPrivate *priv = GSHROOMS_GAIM_SERVER_GET_PRIVATE (server);
	BuddyData *data = (BuddyData *) g_hash_table_lookup (priv->buddydata, receiver);
	GaimConversation *conv;
	gchar *url, *announce;
			
	/* If we don't know the URL yet, wait until next outgoing message */
	url = gshrooms_gaim_server_get_server_url (server);
	g_print ("Got URL: '%s'\n", url);
	if (url == NULL) {
		return;
	}

	/* If we didn't hear anything of the other end, don't do anything yet */
	g_print ("Buddy already replied? %d\n", data != NULL);
	if (data == NULL) {
		return;
	}
	
	/* Don't announce twice */	
	g_print ("Announce already sent? %d\n", data->announce_sent);
	if (data->announce_sent) {
		return;
	}
	
	/* Ok remember we sent the url! */
	data->announce_sent = TRUE;
	gshrooms_gaim_server_send_announce (gaim_account_get_connection (account), receiver, url);
}

gboolean
gshrooms_gaim_server_receiving_im (GaimAccount *account,
								char **sender,
                            	char **buffer,
                            	int *flags,
                            	GShroomsGaimServer *server)
{
	GShroomsGaimServerPrivate *priv = GSHROOMS_GAIM_SERVER_GET_PRIVATE (server);
	gchar *text;
	gboolean hide_message = FALSE;
	
	/* The message is already eaten by another plugin */
	if (buffer == NULL || *buffer == NULL || sender == NULL || *sender == NULL)
		return TRUE;
	
	text = gaim_markup_strip_html (*buffer);
	
	BuddyData *data = (BuddyData *) g_hash_table_lookup (priv->buddydata, *sender);
	if (data == NULL)
	{
		data = g_new0 (BuddyData, 1);		
		g_hash_table_insert (priv->buddydata, g_strdup (*sender), data);
	}
	
	data->active = TRUE;
	if (data->announce_sent && g_str_has_prefix (text, GSHROOMS_GAIM_ACK))
	{		
		data->announce_ack = TRUE;		
		hide_message = TRUE;
	}
	else if (data->announce_sent && g_str_has_prefix (text, GSHROOMS_GAIM_QUIT))
	{
		data->active = FALSE;
		hide_message = TRUE;
	}
	g_free (text);
	
	if (hide_message)
	{
		g_free(*buffer);
		*buffer = NULL;
    }
    
	return hide_message;
}

void
gshrooms_gaim_server_signoff (GaimBuddy *buddy,
                           	GShroomsGaimServer *server)
{
	GShroomsGaimServerPrivate *priv = GSHROOMS_GAIM_SERVER_GET_PRIVATE (server);
	g_hash_table_remove (priv->buddydata, buddy->name);
}

void
gshrooms_gaim_server_conv_del (GaimConversation *conv,
								GShroomsGaimServer *server)
{
	GShroomsGaimServerPrivate *priv = GSHROOMS_GAIM_SERVER_GET_PRIVATE (server);
	BuddyData *data = (BuddyData *) g_hash_table_lookup (priv->buddydata, gaim_conversation_get_name (conv));
	if (data == NULL || !data->announce_ack || !data->active)
		return;
		
	serv_send_im(gaim_conversation_get_gc (conv), gaim_conversation_get_name (conv), GSHROOMS_GAIM_QUIT, 0);
}

static GaimCmdRet
gshrooms_gaim_server_music_command (GaimConversation *conv,
							const gchar *cmd,
							gchar **args,
							gchar **error,
							GShroomsGaimServer *server)
{
	gchar *announce;
	gchar *url = gshrooms_gaim_server_get_server_url (server);
	g_print ("Got URL: '%s'\n", url);
	if (url == NULL) {
		return GAIM_CMD_RET_CONTINUE;
	}
	
	gshrooms_gaim_server_send_announce (gaim_conversation_get_gc (conv),
									gaim_conversation_get_name (conv),
									url);
	
	return GAIM_CMD_RET_OK;
}
