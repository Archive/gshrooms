#ifndef GSHROOMS_GAIM_CLIENT_H
#define GSHROOMS_GAIM_CLIENT_H

#include <glib-object.h>
#include <gaim/conversation.h>
#include <dbus/dbus-glib.h>

G_BEGIN_DECLS

#define GSHROOMS_GAIM_TYPE_CLIENT         (gshrooms_gaim_client_get_type ())
#define GSHROOMS_GAIM_CLIENT(o)           (G_TYPE_CHECK_INSTANCE_CAST ((o), GSHROOMS_GAIM_TYPE_CLIENT, GShroomsGaimClient))
#define GSHROOMS_GAIM_CLIENT_CLASS(k)     (G_TYPE_CHECK_CLASS_CAST((k), GSHROOMS_GAIM_TYPE_CLIENT, GShroomsGaimClientClass))
#define GSHROOMS_GAIM_IS_CLIENT(o)        (G_TYPE_CHECK_INSTANCE_TYPE ((o), GSHROOMS_GAIM_TYPE_CLIENT))
#define GSHROOMS_GAIM_IS_CLIENT_CLASS(k)  (G_TYPE_CHECK_CLASS_TYPE ((k), GSHROOMS_GAIM_TYPE_CLIENT))
#define GSHROOMS_GAIM_CLIENT_GET_CLASS(o) (G_TYPE_INSTANCE_GET_CLASS ((o), GSHROOMS_GAIM_TYPE_CLIENT, GShroomsGaimClientClass))

typedef struct _GShroomsGaimClient 		GShroomsGaimClient;
typedef struct _GShroomsGaimClientClass GShroomsGaimClientClass;

struct _GShroomsGaimClient
{
	GObject parent;
};

struct _GShroomsGaimClientClass
{
	GObjectClass parent;
	DBusGConnection *dbus;
};

GType gshrooms_gaim_client_get_type (void);

gboolean gshrooms_gaim_client_receiving_im (GaimAccount *account,
										char **sender,
										char **buffer,
										int *flags,
										GShroomsGaimClient *client);

void gshrooms_gaim_client_conv_new (GaimConversation *conv,
										GShroomsGaimClient *client);
void gshrooms_gaim_client_conv_del (GaimConversation *conv,
										GShroomsGaimClient *client);
										
void gshrooms_gaim_client_signoff (GaimBuddy *buddy,
										GShroomsGaimClient *client);
										
G_END_DECLS

#endif
