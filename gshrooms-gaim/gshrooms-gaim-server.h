#ifndef GSHROOMS_GAIM_SERVER_H
#define GSHROOMS_GAIM_SERVER_H

#include <glib-object.h>
#include <gaim/conversation.h>
#include <dbus/dbus-glib.h>

G_BEGIN_DECLS

#define GSHROOMS_GAIM_TYPE_SERVER         (gshrooms_gaim_server_get_type ())
#define GSHROOMS_GAIM_SERVER(o)           (G_TYPE_CHECK_INSTANCE_CAST ((o), GSHROOMS_GAIM_TYPE_SERVER, GShroomsGaimServer))
#define GSHROOMS_GAIM_SERVER_CLASS(k)     (G_TYPE_CHECK_CLASS_CAST((k), GSHROOMS_GAIM_TYPE_SERVER, GShroomsGaimServerClass))
#define GSHROOMS_GAIM_IS_SERVER(o)        (G_TYPE_CHECK_INSTANCE_TYPE ((o), GSHROOMS_GAIM_TYPE_SERVER))
#define GSHROOMS_GAIM_IS_SERVER_CLASS(k)  (G_TYPE_CHECK_CLASS_TYPE ((k), GSHROOMS_GAIM_TYPE_SERVER))
#define GSHROOMS_GAIM_SERVER_GET_CLASS(o) (G_TYPE_INSTANCE_GET_CLASS ((o), GSHROOMS_GAIM_TYPE_SERVER, GShroomsGaimServerClass))

typedef struct _GShroomsGaimServer 		GShroomsGaimServer;
typedef struct _GShroomsGaimServerClass GShroomsGaimServerClass;

struct _GShroomsGaimServer
{
	GObject parent;
};

struct _GShroomsGaimServerClass
{
	GObjectClass parent;
	DBusGConnection *dbus;
};

GType gshrooms_gaim_server_get_type (void);

void gshrooms_gaim_server_sending_im (GaimAccount *account,
								const char *receiver,
								char **message,
								GShroomsGaimServer *server);

gboolean gshrooms_gaim_server_receiving_im (GaimAccount *account,
								char **sender,
                            	char **message,
                            	int *flags,
                            	GShroomsGaimServer *server);

void gshrooms_gaim_server_signoff (GaimBuddy *buddy,
                            	GShroomsGaimServer *server);
                            	
void gshrooms_gaim_server_conv_del (GaimConversation *conv,
								GShroomsGaimServer *server);
G_END_DECLS

#endif
