#include "gshrooms-gaim-client.h"
#include "gshrooms-gaim-common.h"

#include <gtk/gtk.h>
#include <gaim/gtkconv.h>
#include <gaim/util.h>

/* DBus method stubs */
#include "rb-buddymusic-source-client.h"
#include "rb-shell-client.h"

#define CONV_OUTPUT(conv, message) (gaim_conversation_write (conv, NULL, message, GAIM_MESSAGE_SYSTEM|GAIM_MESSAGE_RECV, time(NULL)))

#define GSHROOMS_GAIM_CLIENT_GET_PRIVATE(o) (G_TYPE_INSTANCE_GET_PRIVATE ((o), GSHROOMS_GAIM_TYPE_CLIENT, GShroomsGaimClientPrivate))

typedef struct _GShroomsGaimClientPrivate GShroomsGaimClientPrivate;

struct _GShroomsGaimClientPrivate
{
	DBusGProxy *buddymusic_proxy;
	DBusGProxy *rb_proxy;
	GHashTable *buddydata;
};

typedef struct
{
	GShroomsGaimClient *client;
	GaimConversation *conv;
	GaimAccount *account;
	gboolean active;
	gchar *url;	
} BuddyData;

static void gshrooms_gaim_client_init       (GShroomsGaimClient *client);
static void gshrooms_gaim_client_class_init (GShroomsGaimClientClass *klass);
static void gshrooms_gaim_client_finalize	(GObject *object);

static void gshrooms_gaim_client_init_sharing (GShroomsGaimClient *client,
											GaimBuddy *buddy,
											BuddyData *data,
											gboolean add_ui);
								
static void gshrooms_gaim_client_send_buddies (DBusGProxy *proxy,
											gpointer client);
										
G_DEFINE_TYPE (GShroomsGaimClient, gshrooms_gaim_client, G_TYPE_OBJECT);

static void
gshrooms_gaim_client_class_init (GShroomsGaimClientClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);
	
	g_type_class_add_private (klass, sizeof (GShroomsGaimClientPrivate));
	
	object_class->finalize = gshrooms_gaim_client_finalize;
	
	/* This is really supposed to work since we checked at module load */
	klass->dbus = dbus_g_bus_get (DBUS_BUS_SESSION, NULL);
}

static void
free_buddy_data (BuddyData *data)
{
	g_free (data->url);
	g_free (data);
}

static void
gshrooms_gaim_client_init (GShroomsGaimClient *client)
{
	GShroomsGaimClientPrivate *priv = GSHROOMS_GAIM_CLIENT_GET_PRIVATE (client);
	
	/* Proxy to add/remove buddies in rhythmbox */
	priv->buddymusic_proxy = dbus_g_proxy_new_for_name (GSHROOMS_GAIM_CLIENT_GET_CLASS (client)->dbus,
                                                  "org.gnome.Rhythmbox.BuddyMusic",
                                                  "/org/gnome/Rhythmbox/BuddyMusic",
                                                  "org.gnome.Rhythmbox.BuddyMusic");
	dbus_g_proxy_add_signal (priv->buddymusic_proxy, "NeedBuddies", G_TYPE_INVALID);
    dbus_g_proxy_connect_signal (priv->buddymusic_proxy, "NeedBuddies", (GCallback) gshrooms_gaim_client_send_buddies, client, NULL);
	
	priv->rb_proxy = dbus_g_proxy_new_for_name (GSHROOMS_GAIM_CLIENT_GET_CLASS (client)->dbus,
                                                  "org.gnome.Rhythmbox",
                                                  "/org/gnome/Rhythmbox/Shell",
                                                  "org.gnome.Rhythmbox.Shell");
                                                  
	priv->buddydata = g_hash_table_new_full (
							g_str_hash,
							g_str_equal,
							g_free,
							(GDestroyNotify) free_buddy_data);
}

static void
gshrooms_gaim_client_finalize (GObject *object)
{
	GShroomsGaimClient *client;
	GShroomsGaimClientPrivate *priv;
	
	g_return_if_fail (object != NULL);
	g_return_if_fail (GSHROOMS_GAIM_IS_CLIENT (object));

	client = GSHROOMS_GAIM_CLIENT (object);
	priv = GSHROOMS_GAIM_CLIENT_GET_PRIVATE (client);
	
	/* Tell rhythmbox to remove buddies */
	g_list_foreach (gaim_get_ims(), (GFunc) gshrooms_gaim_client_signoff, client);
	
	/* Delete dbus roxy after that */
	g_object_unref (priv->buddymusic_proxy);
	g_object_unref (priv->rb_proxy);
	g_hash_table_destroy (priv->buddydata);
	
	G_OBJECT_CLASS (gshrooms_gaim_client_parent_class)->finalize (object);
}

static void
gshrooms_gaim_client_async_result (DBusGProxy *proxy, GError *error, gpointer data)
{
	if (error != NULL)
	{
		g_print ("Got an error to an async dbus method, tant pis: %s\n", error->message);
		g_error_free (error);
	}
}

static void
gshrooms_gaim_client_send_buddies_foreach (gchar *name,
										BuddyData *data,
										GShroomsGaimClient *client)
{
	gshrooms_gaim_client_init_sharing (client, gaim_find_buddy(data->account, name), data, FALSE);
}

static void
gshrooms_gaim_client_send_buddies (DBusGProxy *proxy, gpointer client)
{
	GShroomsGaimClientPrivate *priv = GSHROOMS_GAIM_CLIENT_GET_PRIVATE (client);
	
	g_hash_table_foreach (priv->buddydata, (GHFunc) gshrooms_gaim_client_send_buddies_foreach, client);
}

static void
gshrooms_gaim_client_rhythmbox_launched (DBusGProxy *proxy, GError *error, gpointer conv)
{
	if (error != NULL)
	{
		if (GAIM_IS_GTK_CONVERSATION (conv)) {
			CONV_OUTPUT ((GaimConversation *)conv, _("Failed to start rhythmbox!"));
		}
		
		g_print ("Failed to start Rhythmbox: %s\n", error->message);
		g_error_free (error);
	}
}

static void
gshrooms_gaim_client_listen (GtkButton *button, BuddyData *data)
{
	CONV_OUTPUT (data->conv, _("Starting rhythmbox..."));
		
	/* Tell rhythmbox to start playing the feed */
	/*
	org_gnome_Rhythmbox_BuddyMusic_play_stream_async (
				GSHROOMS_GAIM_CLIENT_GET_PRIVATE (data->client)->buddymusic_proxy,
				gaim_conversation_get_name (data->conv),
				gshrooms_gaim_client_rhythmbox_launched,
				data->conv);
	*/
	org_gnome_Rhythmbox_Shell_load_ur_i_async (
				GSHROOMS_GAIM_CLIENT_GET_PRIVATE (data->client)->rb_proxy,
				data->url,
				TRUE,
				gshrooms_gaim_client_rhythmbox_launched,
				data->conv);
}

static void
gshrooms_gaim_client_add_listen_ui (GShroomsGaimClient *client, BuddyData *data)
{
	GaimGtkConversation *gtkconv = GAIM_GTK_CONVERSATION(data->conv);
	GtkWidget *button;
			
	g_print ("You can listen to an audio feed !\n");
	button = gaim_gtkconv_button_new (GTK_STOCK_MEDIA_PLAY,
									"Listen",
									"Listen to the music shared by your buddy",
									NULL,
									gshrooms_gaim_client_listen,
									data);
										
	gtk_box_pack_start_defaults (GTK_BOX(gtkconv->bbox), button);
	gtk_widget_show_all (button);
}

static void
gshrooms_gaim_client_update_metadata (GShroomsGaimClient *client,
									GaimConversation *conv,
									gchar **metadata)
{
	GShroomsGaimClientPrivate *priv = GSHROOMS_GAIM_CLIENT_GET_PRIVATE (client);
	gchar *message = NULL;
	
	g_print ("Now Playing:%s|%s|%s\n", metadata[0], metadata[1], metadata[2]);
	
	if (metadata[0] != NULL && *metadata[0] != '\0' && metadata[2] != NULL && *metadata[2] != '\0')
	{
		message = g_strdup_printf (
					/* translators: first %s is artist name, second %s is track title */
					_("Your buddy is starting to play %s - %s"),
					metadata[0],
					metadata[2]);
		g_print ("%s\n", message);
		CONV_OUTPUT (conv, message);
	}
	else if (metadata[2] != NULL && *metadata[2] != '\0')
	{
		message = g_strdup_printf (
					/* translators: %s is track title */
					_("Your buddy is starting to play %s"),
					metadata[2]);
		g_print ("%s\n", message);
		CONV_OUTPUT (conv, message);
	}
	
	/* Tell rhythmbox to update the artist display */
	org_gnome_Rhythmbox_BuddyMusic_update_buddy_async (
				priv->buddymusic_proxy,
				gaim_conversation_get_name (conv),
				metadata[0],
				metadata[1],
				metadata[2],
				gshrooms_gaim_client_async_result,
				client);
	
	g_free (message);
}

static void
gshrooms_gaim_client_init_sharing (GShroomsGaimClient *client,
								GaimBuddy *buddy,
								BuddyData *data,
								gboolean add_ui)
{
	GShroomsGaimClientPrivate *priv = GSHROOMS_GAIM_CLIENT_GET_PRIVATE (client);
	const gchar *icon = gaim_blist_node_get_string ((GaimBlistNode*) buddy, "buddy_icon");
	gchar *cached_icon = NULL;
	gchar *message;
	
	/* If we have a cached favicon, use it as buddy icon */
	if (icon != NULL) {
		cached_icon = g_build_filename (gaim_buddy_icons_get_cache_dir (), icon, NULL);
	}
	
	g_print ("Init Sharing: %s\n", data->url);
	/* Add the buddy in Rhythmbox */
	org_gnome_Rhythmbox_BuddyMusic_add_buddy_async (
				priv->buddymusic_proxy,
				gaim_conversation_get_name (data->conv),
				gaim_buddy_get_contact_alias (buddy),
				cached_icon,
				data->url,
				gshrooms_gaim_client_async_result,
				client);
	
	if (add_ui)
	{
		/* A little message in the conv window */
		message = g_strdup_printf (
					_("You can listen to your buddy's music!"));
		CONV_OUTPUT (data->conv, message);
	
		/* A button pops up */
		gshrooms_gaim_client_add_listen_ui (client, data);
		
		g_free (message);
	}
		
	g_free (cached_icon);
}

static void
gshrooms_gaim_client_ack_announce (GaimConnection *conn,
								const gchar *receiver)
{
	serv_send_im(conn, receiver, GSHROOMS_GAIM_ACK, 0);
}

static void
gshrooms_gaim_client_quit_announce (GaimConnection *conn,
								const gchar *receiver)
{
	serv_send_im(conn, receiver, GSHROOMS_GAIM_QUIT, 0);
}

gboolean
gshrooms_gaim_client_receiving_im (GaimAccount *account,
								char **sender,
								char **buffer,
								int *flags,
								GShroomsGaimClient *client)
{
	GShroomsGaimClientPrivate *priv = GSHROOMS_GAIM_CLIENT_GET_PRIVATE (client);
	GaimConversation *conv;
	GaimBuddy *buddy;
	BuddyData *data;
	gboolean hide_message = FALSE;
	gchar *text;
	
	/* The message is already eaten by another plugin */
	if (buffer == NULL || *buffer == NULL || sender == NULL || *sender == NULL)
		return TRUE;
	
	g_print ("Receiving IM: '%s' : '%s'\n", *sender, *buffer);
		
	/* If the conversation isn't created yet, do nothing, this means it is the
	   very first message from sender coming at us */
	conv = gaim_find_conversation_with_account (*sender, account);
	if (conv == NULL) {
		return FALSE;
	}
	
	buddy = gaim_find_buddy(account, *sender);
	text = gaim_markup_strip_html (*buffer);
	
		
	data = (BuddyData *) g_hash_table_lookup (priv->buddydata, *sender);
	if (data == NULL)
	{
		/* Never heard of that buddy before */
		data = g_new0 (BuddyData, 1);
		data->client = client;
		data->conv = conv;
		data->account = account;
		
		g_hash_table_insert (priv->buddydata, g_strdup (*sender), data);
	}
	data->active = TRUE;
	
	g_print ("Result: '%s' | Announce received ? %d\n", text, data->url != NULL);
	if (data->url == NULL && g_str_has_prefix (text, GSHROOMS_GAIM_PREFIX))
	{
		data->url = g_strdup (g_strrstr (text+GSHROOMS_GAIM_PREFIX_LENGTH, "http://"));
		g_return_val_if_fail (data->url != NULL, FALSE);
		
		/* Do whatever when a new sharing is discovered+show the UI */
		gshrooms_gaim_client_init_sharing (client, buddy, data, TRUE);
		gshrooms_gaim_client_ack_announce (gaim_conversation_get_gc (conv), *sender);
		
		hide_message = TRUE;
	}
	else if (data->url != NULL && g_str_has_prefix (text, GSHROOMS_GAIM_NOWPLAYING))
	{
		gchar **metadata = g_strsplit (text+GSHROOMS_GAIM_NOWPLAYING_LENGTH, "|", 3);
	
		/* Update metadata display */
		gshrooms_gaim_client_update_metadata (client, conv, metadata);
		
		hide_message = TRUE;
		g_strfreev (metadata);
	}
	else if (data->url != NULL && g_str_has_prefix (text, GSHROOMS_GAIM_QUIT))
	{
		data->active = FALSE;
		hide_message = TRUE;
	}
	g_free (text);
	
	if (hide_message)
	{
		g_free(*buffer);
		*buffer = NULL;
    }
    
	return hide_message;
}

void
gshrooms_gaim_client_conv_new (GaimConversation *conv,
							GShroomsGaimClient *client)
{
	GShroomsGaimClientPrivate *priv = GSHROOMS_GAIM_CLIENT_GET_PRIVATE (client);
	
	/* Maybe we had cached infos about the buddy ? */
	BuddyData *data = (BuddyData *) g_hash_table_lookup (
									priv->buddydata,
									gaim_conversation_get_name (conv));
									
	if (data != NULL && data->url != NULL)
	{
		GaimBuddy *buddy = gaim_find_buddy(
					gaim_conversation_get_account (conv),
					gaim_conversation_get_name (conv));
		
		data->conv = conv;
		gshrooms_gaim_client_init_sharing (client, buddy, data, TRUE);
	}
}

void
gshrooms_gaim_client_conv_del (GaimConversation *conv,
							GShroomsGaimClient *client)
{
	GShroomsGaimClientPrivate *priv = GSHROOMS_GAIM_CLIENT_GET_PRIVATE (client);
	
	/* We now ensure the stored data has a NULL conversation instead of garbage pointer */
	BuddyData *data = (BuddyData *) g_hash_table_lookup (
									priv->buddydata,
									gaim_conversation_get_name (conv));
									
	if (data != NULL) {
		data->conv = NULL;
	}
	
	if (data != NULL && data->url != NULL && data->active)
		gshrooms_gaim_client_quit_announce (gaim_conversation_get_gc (conv), gaim_conversation_get_name (conv));
}
		
void
gshrooms_gaim_client_signoff (GaimBuddy *buddy,
							GShroomsGaimClient *client)
{
	GShroomsGaimClientPrivate *priv = GSHROOMS_GAIM_CLIENT_GET_PRIVATE (client);
	
	g_print ("RB Removed %s\n", buddy->name);
	org_gnome_Rhythmbox_BuddyMusic_remove_buddy_async (
				priv->buddymusic_proxy,
				buddy->name,
				gshrooms_gaim_client_async_result,
				client);
	
	g_hash_table_remove (priv->buddydata, buddy->name);
}							
