#include "config.h"
#include "gstnowplaying.h"

#include "gst-nowplaying-server.h"
#include <dbus/dbus-glib-bindings.h>

GST_DEBUG_CATEGORY (nowplaying);	
#define GST_CAT_DEFAULT nowplaying

/* The path to register this gst element on dbus */
#define NOWPLAYING_DBUS_PATH  "/org/gnome/NowPlaying"
#define NOWPLAYING_DBUS_IFACE "org.gnome.NowPlaying"
/* Time to wait after having received the last taglist, before sending dbus signal */
#define NOWPLAYING_TIMEOUT 2000

/* A DBUS type to represent a GHastable of (String, GValue) pairs */
#define DBUS_TYPE_G_STRING_GVALUE_HASHTABLE (dbus_g_type_get_map ("GHashTable", G_TYPE_STRING, G_TYPE_VALUE))

enum {
	NOWPLAYING_SIGNAL,
	NEWMEDIA_SIGNAL,
	LAST_SIGNAL
};
static guint object_signals[LAST_SIGNAL] = {0};

static GstStaticPadTemplate sink_factory =
GST_STATIC_PAD_TEMPLATE (
	"sink",
	GST_PAD_SINK,
	GST_PAD_ALWAYS,
	GST_STATIC_CAPS_ANY
);

static GstStaticPadTemplate src_factory =
GST_STATIC_PAD_TEMPLATE (
	"src",
	GST_PAD_SRC,
	GST_PAD_ALWAYS,
	GST_STATIC_CAPS_ANY
);

static void	gst_nowplaying_class_init	(GstNowPlayingClass *klass);
static void	gst_nowplaying_base_init	(GstNowPlayingClass *klass);
static void	gst_nowplaying_init	(GstNowPlaying *filter);
static void gst_nowplaying_finalize (GObject * object);

static void gst_nowplaying_chain	(GstPad *pad, GstData *data);

static GstElementStateReturn gst_nowplaying_change_state (GstElement * element);

static GstElementClass *parent_class = NULL;

static void
free_and_unset (GValue *val)
{
	g_value_unset (val);
	g_free (val);
}

static void
gst_nowplaying_emit_new_media (GstNowPlaying *filter)
{
	if (filter->table != NULL && g_hash_table_size (filter->table) > 0)
	{
		/* We had a previous table, remove it and emit */
		g_hash_table_destroy (filter->table);
		filter->table = g_hash_table_new_full (
						(GHashFunc)		g_str_hash,
						(GEqualFunc)	g_str_equal,
						(GDestroyNotify)g_free,
						(GDestroyNotify)free_and_unset);
		
		GST_DEBUG ("Emit new media");
		g_signal_emit_by_name (filter, "new-media");
	}
	else if (filter->table == NULL)
	{
		/* The previous table was already blank, don't emit */
		filter->table = g_hash_table_new_full (
						(GHashFunc)		g_str_hash,
						(GEqualFunc)	g_str_equal,
						(GDestroyNotify)g_free,
						(GDestroyNotify)free_and_unset);
	}	
}

static gboolean
gst_nowplaying_emit_now_playing_timeout (GstNowPlaying *filter)
{
	if (filter->table == NULL) {
		g_assert_not_reached ();
	}
	
	GST_DEBUG ("Emit now playing");
	g_signal_emit_by_name (filter, "now-playing", filter->table);
	return FALSE;
}

static void
gst_nowplaying_emit_now_playing (GstNowPlaying *filter, gboolean send_now)
{
	if (send_now)
	{
		GST_DEBUG ("Sending Now NowPlaying");
		gst_nowplaying_emit_now_playing_timeout (filter);
	}
	else
	{
		/* We reset the timer */
		if (filter->timeout_id != 0)
			g_source_remove (filter->timeout_id);
		GST_DEBUG ("Sending NowPlaying in %d", NOWPLAYING_TIMEOUT);
		filter->timeout_id = g_timeout_add (NOWPLAYING_TIMEOUT, (GSourceFunc) gst_nowplaying_emit_now_playing_timeout, filter);
	}
}

static void
copy_metadata (gchar *key, GValue *value, GHashTable *metadata)
{
	GValue *copy_value;

	copy_value = g_new0 (GValue, 1);
	g_value_init (copy_value, G_VALUE_TYPE (value));
	g_value_copy (value, copy_value);
	
	g_hash_table_insert (metadata, g_strdup (key), copy_value);
}

gboolean
gst_nowplaying_get_now_playing (GstNowPlaying *filter, GHashTable **metadata, GError **error)
{
	GST_DEBUG ("Someone asks for nowplying data");
	*metadata = g_hash_table_new_full (
						(GHashFunc)		g_str_hash,
						(GEqualFunc)	g_str_equal,
						(GDestroyNotify)g_free,
						(GDestroyNotify)free_and_unset);
	g_hash_table_foreach (filter->table, (GHFunc) copy_metadata, *metadata);
	return TRUE;
}

static void
gst_nowplaying_init_klass_dbus (GstNowPlayingClass *klass)
{
	GError *error = NULL;
	DBusGProxy *driver_proxy;
	
	klass->connection = dbus_g_bus_get (DBUS_BUS_SESSION, &error);
	if (klass->connection == NULL)
	{
		g_warning("Unable to connect to dbus: %s", error->message);
		g_error_free (error);
		return;
	}
}

GType
gst_nowplaying_get_type (void)
{
	static GType plugin_type = 0;

	if (!plugin_type)
	{
		static const GTypeInfo plugin_info =
		{
			sizeof (GstNowPlayingClass),
			(GBaseInitFunc) gst_nowplaying_base_init,
			NULL,
			(GClassInitFunc) gst_nowplaying_class_init,
			NULL,
			NULL,
			sizeof (GstNowPlaying),
			0,
			(GInstanceInitFunc) gst_nowplaying_init,
		};
		plugin_type = g_type_register_static (GST_TYPE_ELEMENT,"GstNowPlaying",&plugin_info, 0);
	}
	return plugin_type;
}

/*
 * Connect pads and set details, everything passes through us
 */
static void
gst_nowplaying_base_init (GstNowPlayingClass *klass)
{
	static const GstElementDetails plugin_details = {
		"Now Playing",
		"Generic/Metadata/Parse",
		"Send metadata over dbus",
		"Raphael Slinckx <raphael@slinckx.net>"
	};
	GstElementClass *element_class = GST_ELEMENT_CLASS (klass);

	gst_element_class_add_pad_template (element_class,
	gst_static_pad_template_get (&src_factory));

	gst_element_class_add_pad_template (element_class,
	gst_static_pad_template_get (&sink_factory));

	gst_element_class_set_details (element_class, &plugin_details);
}

static void
gst_nowplaying_class_init (GstNowPlayingClass *klass)
{
	GObjectClass *gobject_class;
	GstElementClass *gstelement_class;
	
	gobject_class = (GObjectClass*) klass;
	gstelement_class = (GstElementClass*) klass;

	parent_class = g_type_class_ref (GST_TYPE_ELEMENT);
	
	gobject_class->finalize = GST_DEBUG_FUNCPTR (gst_nowplaying_finalize);
	
	gstelement_class->change_state = GST_DEBUG_FUNCPTR (gst_nowplaying_change_state);
	
	/* Init the dbus connection per-class */
	gst_nowplaying_init_klass_dbus(klass);

	/* Signals */	
	object_signals[NOWPLAYING_SIGNAL] =
		g_signal_new ("now-playing",
			G_TYPE_FROM_CLASS (gobject_class),
			(GSignalFlags)(G_SIGNAL_RUN_LAST),
			G_STRUCT_OFFSET (GstNowPlayingClass, now_playing),
			NULL, NULL,
			g_cclosure_marshal_VOID__BOXED,
			G_TYPE_NONE, 1,
			DBUS_TYPE_G_STRING_GVALUE_HASHTABLE);

	object_signals[NEWMEDIA_SIGNAL] =
		g_signal_new ("new-media",
			G_TYPE_FROM_CLASS (gobject_class),
			(GSignalFlags)(G_SIGNAL_RUN_LAST),
			G_STRUCT_OFFSET (GstNowPlayingClass, new_media),
			NULL, NULL,
			g_cclosure_marshal_VOID__VOID,
			G_TYPE_NONE, 0);
			
	klass->now_playing = NULL;
	klass->new_media = NULL;

	/* Register class info */
	dbus_g_object_type_install_info (GST_TYPE_NOWPLAYING, &dbus_glib_gst_nowplaying_object_info);
}

/*
 * Add pads, everything passes untouched, we also setup chain function and get events
 */
static void
gst_nowplaying_init (GstNowPlaying *filter)
{
	GstElementClass *gstelement_class = GST_ELEMENT_GET_CLASS (filter);
	GstNowPlayingClass *nowplaying_klass = GST_NOWPLAYING_GET_CLASS (filter);
	GError *error = NULL;
	DBusGProxy *driver_proxy;
	guint32 request_name_ret;
  
	filter->sinkpad = gst_pad_new_from_template (gst_element_class_get_pad_template (gstelement_class, "sink"), "sink");
	gst_pad_set_link_function (filter->sinkpad, gst_pad_proxy_pad_link);
	gst_pad_set_getcaps_function (filter->sinkpad, gst_pad_proxy_getcaps);

	filter->srcpad = gst_pad_new_from_template (gst_element_class_get_pad_template (gstelement_class, "src"), "src");
	gst_pad_set_link_function (filter->sinkpad, gst_pad_proxy_pad_link);
	gst_pad_set_getcaps_function (filter->srcpad, gst_pad_proxy_getcaps);

	gst_element_add_pad (GST_ELEMENT (filter), filter->sinkpad);
	gst_element_add_pad (GST_ELEMENT (filter), filter->srcpad);

	gst_pad_set_chain_function (filter->sinkpad, gst_nowplaying_chain);
	
	GST_FLAG_SET (filter, GST_ELEMENT_EVENT_AWARE);
	GST_FLAG_SET (filter, GST_ELEMENT_WORK_IN_PLACE);
	
	/* Init the hastable */
	filter->table = NULL;
	
	/* No timeout yet */
	filter->timeout_id = 0;
			
	/* Connect instance to dbus */
	g_return_if_fail (nowplaying_klass->connection != NULL);
	
	GST_DEBUG ("Registering object: "NOWPLAYING_DBUS_PATH);
	dbus_g_connection_register_g_object (nowplaying_klass->connection,
										NOWPLAYING_DBUS_PATH,
										G_OBJECT (filter));

    /* Register the service name org.gnome.NowPlaying */
    driver_proxy = dbus_g_proxy_new_for_name (nowplaying_klass->connection,
                                            DBUS_SERVICE_DBUS,
                                            DBUS_PATH_DBUS,
                                            DBUS_INTERFACE_DBUS);

	if(!org_freedesktop_DBus_request_name (driver_proxy, NOWPLAYING_DBUS_IFACE, 0, &request_name_ret, &error))
	{
		GST_DEBUG("Unable to register service: %s", error->message);
		g_error_free (error);
		return;
	}
	g_object_unref (driver_proxy);
	
	gst_nowplaying_emit_new_media (filter);
}

static void
gst_nowplaying_finalize (GObject * object)
{
	GstNowPlaying *filter = GST_NOWPLAYING (object);

	g_hash_table_destroy (filter->table);
	
	if (filter->timeout_id != 0)
		g_source_remove (filter->timeout_id);

	G_OBJECT_CLASS (parent_class)->finalize (object);
}

                                   
/*
 * For each tag in the taglist. We send it on dbus.
 */
static void
gst_nowplaying_taglist (const GstTagList *list, const gchar *tag, GstNowPlaying *now_playing)
{
	GValue *val;
	guint size;
	gchar *value;
	
	//GST_DEBUG("Got tag: %s, (%s: %s)", tag, gst_tag_get_nick (tag), gst_tag_get_description (tag));
		
	size = gst_tag_list_get_tag_size (list, tag);
	//TODO: handle multiple values
	if (size > 0)
	{
		/* FIXME: debug */
		value = g_strdup_value_contents (gst_tag_list_get_value_index(list, tag, 0));
		//GST_DEBUG("\tValue: %s", value);
		g_free (value);
		
		val = g_new0 (GValue, 1);
		g_value_init (val, gst_tag_get_type (tag));
		g_value_copy (gst_tag_list_get_value_index(list, tag, 0), val);
					
		g_hash_table_insert (now_playing->table, g_strdup(tag), val);		
	}
}

/*
 * Handle an event, we passe everything to default handler, except tags
 * which are prepared to be sent on dbus.
 */
static void
gst_nowplaying_event (GstPad * pad, GstEvent * event)
{
	GstNowPlaying *now_playing = GST_NOWPLAYING (GST_PAD_PARENT (pad));
	
	switch (GST_EVENT_TYPE (event))
	{
		case GST_EVENT_TAG:
			GST_DEBUG ("Got tag!");
			/* Got a tag, we are going to handle it */
			gst_tag_list_foreach(gst_event_tag_get_list(event), (GstTagForeachFunc) gst_nowplaying_taglist, now_playing);
		    
		    gst_nowplaying_emit_now_playing (now_playing, FALSE);
		    break;
		case GST_EVENT_EOS:
			gst_nowplaying_emit_new_media (now_playing);
			break;			
		default:
		    break;
	}
	
    /* Do default stuff too */
	gst_pad_event_default (pad, event);
}

/*
 * Chain function, if we have an event, handle it in _event function,
 * if it's a buffer, simply pass it forward.
 */
static void
gst_nowplaying_chain (GstPad *pad, GstData *data)
{
	GstNowPlaying *filter;
	GstBuffer *buf = GST_BUFFER (data);

	g_return_if_fail (GST_IS_PAD (pad));
	g_return_if_fail (buf != NULL);

	filter = GST_NOWPLAYING (GST_OBJECT_PARENT (pad));
	g_return_if_fail (GST_IS_NOWPLAYING (filter));

	if (GST_IS_EVENT (buf)) {
		gst_nowplaying_event (pad, GST_EVENT (buf));
	}
	else
	{
		gst_pad_push (filter->srcpad, GST_DATA (buf));
	}
}

static GstElementStateReturn
gst_nowplaying_change_state (GstElement * element)
{
	GstNowPlaying *filter;

	g_return_val_if_fail (GST_IS_NOWPLAYING (element), GST_STATE_FAILURE);
	filter = GST_NOWPLAYING (element);

	switch (GST_STATE_TRANSITION (element)) {
		case GST_STATE_NULL_TO_READY:
		case GST_STATE_READY_TO_PAUSED:
		case GST_STATE_PAUSED_TO_PLAYING:
		case GST_STATE_PLAYING_TO_PAUSED:
			break;
		case GST_STATE_PAUSED_TO_READY:
			gst_nowplaying_emit_new_media (filter);
			break;
		case GST_STATE_READY_TO_NULL:
		default:
			break;
	}

	if (GST_ELEMENT_CLASS (parent_class)->change_state)
		return GST_ELEMENT_CLASS (parent_class)->change_state (element);

	return GST_STATE_SUCCESS;
}

/*
 * Init plugin registration
 */
static gboolean
plugin_init (GstPlugin *plugin)
{
	GST_DEBUG_CATEGORY_INIT (nowplaying, "nowplaying", 0, "DBus Now Playing");
	return gst_element_register (plugin, "nowplaying", GST_RANK_NONE, GST_TYPE_NOWPLAYING);
}

//TODO: Correct url here

/* Plugin details */
GST_PLUGIN_DEFINE (
  GST_VERSION_MAJOR,
  GST_VERSION_MINOR,
  "nowplaying",
  "Send metadata over dbus",
  plugin_init,
  VERSION,
  "LGPL",
  "nowplaying",
  "http://raphael.slinckx.net/"
)
