#ifndef __GST_NOWPLAYING_H__
#define __GST_NOWPLAYING_H__

#include <gst/gst.h>

#define DBUS_API_SUBJECT_TO_CHANGE
#include <dbus/dbus-glib.h>

#include <glib.h>

G_BEGIN_DECLS

/* #define's don't like whitespacey bits */
#define GST_TYPE_NOWPLAYING \
	(gst_nowplaying_get_type())
#define GST_NOWPLAYING(obj) \
	(G_TYPE_CHECK_INSTANCE_CAST((obj),GST_TYPE_NOWPLAYING,GstNowPlaying))
#define GST_NOWPLAYING_CLASS(klass) \
	(G_TYPE_CHECK_CLASS_CAST((klass),GST_TYPE_NOWPLAYING,GstNowPlaying))
#define GST_IS_NOWPLAYING(obj) \
	(G_TYPE_CHECK_INSTANCE_TYPE((obj),GST_TYPE_NOWPLAYING))
#define GST_IS_NOWPLAYING_CLASS(obj) \
	(G_TYPE_CHECK_CLASS_TYPE((klass),GST_TYPE_NOWPLAYING))
#define GST_NOWPLAYING_GET_CLASS(obj) \
	(G_TYPE_INSTANCE_GET_CLASS((obj), GST_TYPE_NOWPLAYING, GstNowPlayingClass))

typedef struct _GstNowPlaying      GstNowPlaying;
typedef struct _GstNowPlayingClass GstNowPlayingClass;

struct _GstNowPlaying
{
	GstElement element;
	GstPad *sinkpad, *srcpad;
	
	GHashTable *table;
	guint timeout_id;
};

struct _GstNowPlayingClass 
{
	GstElementClass parent_class;
	DBusGConnection *connection;
	
	/*signal*/
	void (* now_playing) (GstNowPlaying *self, GHashTable *table);
	void (* new_media) (GstNowPlaying *self);
};

GType gst_nowplaying_get_type (void);

gboolean gst_nowplaying_get_now_playing (GstNowPlaying *self,
										GHashTable **metadata,
										GError **error);

G_END_DECLS

#endif /* __GST_NOWPLAYING_H__ */
