#include "config.h"
#include "gstnowplayingwriter.h"

#include "gst-nowplaying-client.h"

GST_DEBUG_CATEGORY (nowplayingwriter);	
#define GST_CAT_DEFAULT nowplayingwriter

/* Dbus paths, etc */
#define NOWPLAYING_DBUS_PATH  "/org/gnome/NowPlaying"
#define NOWPLAYING_DBUS_IFACE "org.gnome.NowPlaying"
/* A DBUS type to represent a GHastable of (String, GValue) pairs */
#define DBUS_TYPE_G_STRING_GVALUE_HASHTABLE (dbus_g_type_get_map ("GHashTable", G_TYPE_STRING, G_TYPE_VALUE))

static GstStaticPadTemplate sink_factory =
GST_STATIC_PAD_TEMPLATE (
	"sink",
	GST_PAD_SINK,
	GST_PAD_ALWAYS,
	GST_STATIC_CAPS_ANY
);

static GstStaticPadTemplate src_factory =
GST_STATIC_PAD_TEMPLATE (
	"src",
	GST_PAD_SRC,
	GST_PAD_ALWAYS,
	GST_STATIC_CAPS_ANY
);

static void	gst_nowplayingwriter_class_init	(GstNowPlayingWriterClass *klass);
static void	gst_nowplayingwriter_base_init	(GstNowPlayingWriterClass *klass);
static void	gst_nowplayingwriter_init	(GstNowPlayingWriter *filter);
static void gst_nowplaying_finalize (GObject * object);

static void gst_nowplayingwriter_chain	(GstPad *pad, GstData *data);

static GstElementStateReturn gst_nowplayingwriter_change_state (GstElement * element);

static GstElementClass *parent_class = NULL;

static void
gst_nowplayingwriter_build_list (gchar *key, GValue *data, GstTagList *list)
{
	gchar *print = g_strdup_value_contents (data);
	GST_DEBUG ("Got Tag, '%s' : '%s'", key, print);
	g_free (print);
	
    if (gst_tag_get_flag (key) == GST_TAG_FLAG_META)
	{
		GST_DEBUG("\tThat was a meta tag!");
		gst_tag_list_add_values (list, GST_TAG_MERGE_APPEND, key, data, NULL);
	}
}

static void
gst_nowplayingwriter_now_playing (DBusGProxy *proxy, GHashTable *table, GstNowPlayingWriter *filter)
{
    if (filter->tags != NULL)
	{
		gst_tag_list_free (filter->tags);
	}
	
	filter->tags = gst_tag_list_new ();
    GST_DEBUG ("Got dbus signal, decrypting");
    
    g_hash_table_foreach (table, (GHFunc) gst_nowplayingwriter_build_list, filter->tags);
}

static void
gst_nowplayingwriter_new_media (DBusGProxy *proxy, GstNowPlayingWriter *filter)
{
    GST_DEBUG ("Got dbus newmedia, decrypting");
    if (filter->tags != NULL)
	{
		gst_tag_list_free (filter->tags);
	}
}

static void
gst_nowplayingwriter_ask_nowplaying (GstNowPlayingWriter *filter)
{
	GError *err = NULL;
	GHashTable *metadata;
	
	GST_DEBUG ("Null to Ready");
	if (!org_gnome_NowPlaying_get_now_playing (filter->nowplaying, &metadata, &err))
	{
		GST_DEBUG("Unable to retreive now playing song: %s", err->message);
		g_error_free (err);
	}
	else
	{
		gst_nowplayingwriter_now_playing (filter->nowplaying, metadata, filter);
		g_hash_table_destroy (metadata);
	}
}

static void
gst_nowplayingwriter_init_klass_dbus (GstNowPlayingWriterClass *klass)
{
	GError *error = NULL;
	
	klass->connection = dbus_g_bus_get (DBUS_BUS_SESSION, &error);
	if (klass->connection == NULL)
	{
		GST_DEBUG("Unable to connect to dbus: %s", error->message);
		g_error_free (error);
		return;
	}
}

GType
gst_nowplayingwriter_get_type (void)
{
	static GType plugin_type = 0;

	if (!plugin_type)
	{
		static const GTypeInfo plugin_info =
		{
			sizeof (GstNowPlayingWriterClass),
			(GBaseInitFunc) gst_nowplayingwriter_base_init,
			NULL,
			(GClassInitFunc) gst_nowplayingwriter_class_init,
			NULL,
			NULL,
			sizeof (GstNowPlayingWriter),
			0,
			(GInstanceInitFunc) gst_nowplayingwriter_init,
		};
		plugin_type = g_type_register_static (GST_TYPE_ELEMENT,"GstNowPlayingWriter",&plugin_info, 0);
	}
	return plugin_type;
}

/*
 * Connect pads and set details, everything passes through us
 */
static void
gst_nowplayingwriter_base_init (GstNowPlayingWriterClass *klass)
{
	static const GstElementDetails plugin_details = {
		"Now Playing Writer",
		"Generic/Metadata/Parse",
		"Write metadata received from dbus",
		"Raphael Slinckx <raphael@slinckx.net>"
	};
	GstElementClass *element_class = GST_ELEMENT_CLASS (klass);

	gst_element_class_add_pad_template (element_class,
	gst_static_pad_template_get (&src_factory));

	gst_element_class_add_pad_template (element_class,
	gst_static_pad_template_get (&sink_factory));

	gst_element_class_set_details (element_class, &plugin_details);
}

static void
gst_nowplayingwriter_class_init (GstNowPlayingWriterClass *klass)
{
	GObjectClass *gobject_class;
	GstElementClass *gstelement_class;
	
	gobject_class = (GObjectClass*) klass;
	gstelement_class = (GstElementClass*) klass;

	parent_class = g_type_class_ref (GST_TYPE_ELEMENT);
	
	gstelement_class->change_state = GST_DEBUG_FUNCPTR (gst_nowplayingwriter_change_state);
	
	/* Init the dbus connection per-class */
	gst_nowplayingwriter_init_klass_dbus(klass);
}

/*
 * Add pads, everything passes untouched, we also setup chain function and get events
 */
static void
gst_nowplayingwriter_init (GstNowPlayingWriter *filter)
{
	GstElementClass *gstelement_class = GST_ELEMENT_GET_CLASS (filter);
	GstNowPlayingWriterClass *nowplayingwriter_klass = GST_NOWPLAYINGWRITER_GET_CLASS (filter);
	DBusGProxy *proxy;
	
	filter->sinkpad = gst_pad_new_from_template (gst_element_class_get_pad_template (gstelement_class, "sink"), "sink");
	gst_pad_set_link_function (filter->sinkpad, gst_pad_proxy_pad_link);
	gst_pad_set_getcaps_function (filter->sinkpad, gst_pad_proxy_getcaps);

	filter->srcpad = gst_pad_new_from_template (gst_element_class_get_pad_template (gstelement_class, "src"), "src");
	gst_pad_set_link_function (filter->sinkpad, gst_pad_proxy_pad_link);
	gst_pad_set_getcaps_function (filter->srcpad, gst_pad_proxy_getcaps);

	gst_element_add_pad (GST_ELEMENT (filter), filter->sinkpad);
	gst_element_add_pad (GST_ELEMENT (filter), filter->srcpad);

	gst_pad_set_chain_function (filter->sinkpad, gst_nowplayingwriter_chain);
	
	GST_FLAG_SET (filter, GST_ELEMENT_WORK_IN_PLACE);
	
	/* Defaults */
	filter->tags = NULL;
	
	/* Register for GetNowPlayingWriter signals. TODO: use bindings STRING:GVALUE for these when available */
	g_return_if_fail (nowplayingwriter_klass->connection != NULL);
    filter->nowplaying = dbus_g_proxy_new_for_name (nowplayingwriter_klass->connection,
													NOWPLAYING_DBUS_IFACE,
													NOWPLAYING_DBUS_PATH,
													NOWPLAYING_DBUS_IFACE);
	dbus_g_proxy_add_signal (filter->nowplaying, "NowPlaying", DBUS_TYPE_G_STRING_GVALUE_HASHTABLE, G_TYPE_INVALID);
	dbus_g_proxy_add_signal (filter->nowplaying, "NewMedia", G_TYPE_INVALID);
    
    dbus_g_proxy_connect_signal (filter->nowplaying, "NowPlaying", (GCallback) gst_nowplayingwriter_now_playing, filter, NULL);
    dbus_g_proxy_connect_signal (filter->nowplaying, "NewMedia", (GCallback) gst_nowplayingwriter_new_media, filter, NULL);
}

static void
gst_nowplaying_finalize (GObject * object)
{
	GstNowPlayingWriter *filter = GST_NOWPLAYINGWRITER (object);

	if (filter->nowplaying != NULL)
        g_object_unref (filter->nowplaying);
    
    if (filter->tags != NULL)
        gst_tag_list_free (filter->tags);

	G_OBJECT_CLASS (parent_class)->finalize (object);
}

/*
 * Simply pass the buffer forward.
 */
static void
gst_nowplayingwriter_chain (GstPad *pad, GstData *data)
{
	GstNowPlayingWriter *filter;

	g_return_if_fail (GST_IS_PAD (pad));

	filter = GST_NOWPLAYINGWRITER (GST_OBJECT_PARENT (pad));
	g_return_if_fail (GST_IS_NOWPLAYINGWRITER (filter));

	if (filter->tags != NULL)
	{
		/* We have a pending tag, send it */
		gst_element_found_tags_for_pad  (GST_ELEMENT (filter), filter->srcpad, 0, filter->tags);
		filter->tags = NULL;
	}
	
	gst_pad_push (filter->srcpad, data);
}

static GstElementStateReturn
gst_nowplayingwriter_change_state (GstElement * element)
{
	GstNowPlayingWriter *filter;

	g_return_val_if_fail (GST_IS_NOWPLAYINGWRITER (element), GST_STATE_FAILURE);
	filter = GST_NOWPLAYINGWRITER (element);

	switch (GST_STATE_TRANSITION (element)) {
		case GST_STATE_NULL_TO_READY:
			gst_nowplayingwriter_ask_nowplaying (filter);
			break;
		case GST_STATE_READY_TO_PAUSED:
			break;
		case GST_STATE_PAUSED_TO_PLAYING:
			gst_nowplayingwriter_ask_nowplaying (filter);
			break;
		case GST_STATE_PLAYING_TO_PAUSED:
		case GST_STATE_PAUSED_TO_READY:
		case GST_STATE_READY_TO_NULL:
			break;
		default:
			break;
	}

	if (GST_ELEMENT_CLASS (parent_class)->change_state)
		return GST_ELEMENT_CLASS (parent_class)->change_state (element);

	return GST_STATE_SUCCESS;
}

/*
 * Init plugin registration
 */
static gboolean
plugin_init (GstPlugin *plugin)
{
	GST_DEBUG_CATEGORY_INIT (nowplayingwriter, "nowplayingwriter", 0, "DBus Now Playing Tag Writer");
	return gst_element_register (plugin, "nowplayingwriter", GST_RANK_NONE, GST_TYPE_NOWPLAYINGWRITER);
}

//TODO: Correct url here

/* Plugin details */
GST_PLUGIN_DEFINE (
  GST_VERSION_MAJOR,
  GST_VERSION_MINOR,
  "nowplayingwriter",
  "Write metadata received from dbus",
  plugin_init,
  VERSION,
  "LGPL",
  "nowplayingwriter",
  "http://raphael.slinckx.net/"
)
