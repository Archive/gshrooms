#ifndef __GST_NOWPLAYINGWRITER_H__
#define __GST_NOWPLAYINGWRITER_H__

#include <gst/gst.h>

#define DBUS_API_SUBJECT_TO_CHANGE
#include <dbus/dbus-glib.h>

#include <glib.h>

G_BEGIN_DECLS

/* #define's don't like whitespacey bits */
#define GST_TYPE_NOWPLAYINGWRITER \
	(gst_nowplayingwriter_get_type())
#define GST_NOWPLAYINGWRITER(obj) \
	(G_TYPE_CHECK_INSTANCE_CAST((obj),GST_TYPE_NOWPLAYINGWRITER,GstNowPlayingWriter))
#define GST_NOWPLAYINGWRITER_CLASS(klass) \
	(G_TYPE_CHECK_CLASS_CAST((klass),GST_TYPE_NOWPLAYINGWRITER,GstNowPlayingWriter))
#define GST_IS_NOWPLAYINGWRITER(obj) \
	(G_TYPE_CHECK_INSTANCE_TYPE((obj),GST_TYPE_NOWPLAYINGWRITER))
#define GST_IS_NOWPLAYINGWRITER_CLASS(obj) \
	(G_TYPE_CHECK_CLASS_TYPE((klass),GST_TYPE_NOWPLAYINGWRITER))
#define GST_NOWPLAYINGWRITER_GET_CLASS(obj) \
	(G_TYPE_INSTANCE_GET_CLASS((obj), GST_TYPE_NOWPLAYINGWRITER, GstNowPlayingWriterClass))

typedef struct _GstNowPlayingWriter      GstNowPlayingWriter;
typedef struct _GstNowPlayingWriterClass GstNowPlayingWriterClass;

struct _GstNowPlayingWriter
{
	GstElement element;
	GstPad *sinkpad, *srcpad;
	
	GstTagList *tags;
	DBusGProxy *nowplaying;
};

struct _GstNowPlayingWriterClass 
{
	GstElementClass parent_class;
	DBusGConnection *connection;
};

GType gst_nowplayingwriter_get_type (void);

G_END_DECLS

#endif /* __GST_NOWPLAYINGWRITER_H__ */
