import dbus
import dbus.service
if getattr(dbus, 'version', (0,0,0)) >= (0,41,0):
	import dbus.glib

import gobject

from settings import NOWPLAYING_DBUS_IFACE, NOWPLAYING_DBUS_PATH

class NowPlaying(gobject.GObject):
	__gsignals__ = {
		# A new metadata dict is ready
		'now-playing':	(gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE,
						()),
		# The player has a new or no more songs
		'new-media':		(gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE,
						())
	}
	
	def __init__(self):
		gobject.GObject.__init__(self)
		bus = dbus.SessionBus()
		
		# Even if there is no service yet, we want to register,
		# hence the empty string
		bus.add_signal_receiver (
			self._on_now_playing,
			'NowPlaying',
			NOWPLAYING_DBUS_IFACE,
			'',
			NOWPLAYING_DBUS_PATH)

		bus.add_signal_receiver (
			self._on_new_media,
			'NewMedia',
			NOWPLAYING_DBUS_IFACE,
			'',
			NOWPLAYING_DBUS_PATH)

		self.infos = {}
		
		if dbus.version >= (0, 50, 0):
			try:
				nowplaying_obj = bus.get_object(NOWPLAYING_DBUS_IFACE, NOWPLAYING_DBUS_PATH)
				nowplaying_proxy = dbus.Interface(nowplaying_obj, NOWPLAYING_DBUS_IFACE)
				nowplaying_proxy.GetNowPlaying(reply_handler=self._got_now_playing, error_handler=self._got_no_now_playing)
			except Exception, msg:
				print 'Warning:', msg
	
	def get_infos(self):
		return self.infos
		
	def _on_now_playing(self, infos):
		self.infos = infos
		gobject.GObject.emit(self, 'now-playing')
	
	def _on_new_media(self):
		try:
			self.infos = {}
			gobject.GObject.emit(self, 'new-media')
		except KeyError:
			pass
	
	def _got_now_playing(self, infos):
		self.infos = infos
		
	def _got_no_now_playing(self, error):
		print 'Warning:', error
		
gobject.type_register(NowPlaying)

if __name__ == "__main__":
	import gtk
	
	def update(now_playing):
		print p.get_infos()
		
	def delete(now_playing):
		print 'new song'
		
	p = NowPlaying()
	p.connect("now-playing", update)
	p.connect("new-media", delete)
	gtk.main()

