#Twisted
if __name__ == "__main__":
	from twisted.internet import gtk2reactor
	gtk2reactor.install()
from twisted.internet import reactor

import gconf, gobject, sys, os, tempfile, random

# Our modules
from remotecontrol import RemoteControl
from nowplaying import NowPlaying
from natopen import NatOpen
from httpstreamingserver import HttpStreamingServer

from settings import STREAMER_SHUTDOWN_TIMEOUT
from settings import PIPELINE_GCONF, SOURCE_GCONF
from settings import SOURCE_PLAYER

class Streamer:
	def __init__(self):
		self.server = HttpStreamingServer(maxclients=3)
			
		# Store the IP Address and port we are listening on
		self.net_info = None
		# Store the server state
		self.active = False
		self.keepalive = 0
		# We have a shutdown timeout
		if "--debug" not in sys.argv:
			self.shutdown_timeout = gobject.timeout_add(STREAMER_SHUTDOWN_TIMEOUT, self._on_shutdown_timeout)
		
		#Socket name we wil use to connect to RB
		#FIXME: We don't use unix sockets until gstreamer 0.9
		#self.socket_name = os.path.join(tempfile.gettempdir(), "rb-sound-%05d" % random.randint(0,100000))
		
		#Listen for now playing dbus events
		self.nowplaying = NowPlaying()

		#Init remote control
		self.remotecontrol = RemoteControl(self)
		
		#Init NAT opener
		self.natopen = NatOpen(self.server)

		# Init gconf client
		self.client = gconf.client_get_default()
		self.client.add_dir("/apps/gshrooms", gconf.CLIENT_PRELOAD_RECURSIVE)
		
		# Everything is setup, connect the signals now --------------
		# Watch prefs change
		self.client.notify_add("/apps/gshrooms/source", lambda x,y,z,a: self._on_source_change(z.value))
		self.client.notify_add("/apps/gshrooms/pipeline", lambda x,y,z,a: self._on_pipeline_change(z.value))
		self._on_source_change(self.client.get("/apps/gshrooms/source"))
		self._on_pipeline_change(self.client.get("/apps/gshrooms/pipeline"))
		
		# Watch server state change
		self.server.connect("start-streaming", self._on_start_streaming)
		self.server.connect("stop-streaming", self._on_stop_streaming)
				
		# Metadata from dbus	
		self.nowplaying.connect('now-playing', self._on_update_now_playing)
		self.nowplaying.connect('new-media', self._on_update_now_playing)
		
		# Map ports
		self.natopen.open_ports().addCallback(
				self._set_net_info).addErrback(
				self._set_net_info_error)
		
		# FIXME: Debug only
		gobject.timeout_add(10000, self.ping)
				
	def get_net_info(self):
		if self.net_info != None:
			return self.net_info
		else:
			return (False, "", 0)
	
	def get_socket_name(self):
		return self.socket_name
		
	def shutdown(self):
		print 'Shutting down streamer.py'
		gobject.source_remove(self.shutdown_timeout)
		self.server.stop()
		self.natopen.close_ports().addBoth(lambda x:reactor.stop())
	
	def ask_shutdown(self):
		self.keepalive = 1
		
	def keep_alive(self):
		print 'Keep Alive'
		self.keepalive = self.keepalive+1
	
	def ping(self):
		# Debug
		print 'Pong'
		return True
		
	# Private Methods	
	def _on_shutdown_timeout(self):
		if self.active:
			# Server is active, ignore the shutdown
			return True
		elif self.keepalive > 0:
			# Server is inactive but we have a pending keepalive
			self.keepalive = self.keepalive-1
			return True
		else:
			# Server is inactive and we have no more keepalives pending
			self.shutdown()
			return False
		
	def _on_update_now_playing(self, nowplaying):
		print 'Nowplaying: ', nowplaying
		self.server.set_metadata(nowplaying)
								
	def _on_start_streaming(self, server):
		print 'Started streaming'
		self.active = True
			
	def _on_stop_streaming(self, server):
		print 'Stopped Steaming'
		self.active = False
	
	def _set_net_info(self, info):
		print 'Got net infos:', info
		self.net_info = info
		
	def _set_net_info_error(self, error):
		print 'Could not get net infos:', error
		self.net_info = error
	
	def _on_source_change(self, value):
		print 'Got GConf source change', value
		if value != None and value.type == gconf.VALUE_INT:
			value = value.get_int()
# FIXME: We don't use unix sockets until gstreamer 0.9
#			if value == SOURCE_PLAYER:
#				#value = SOURCE_GCONF[value] + ' address="%s" socket-type=1' % self.socket_name
#				value = "tcpclientsrc address=/tmp/test socket-type=1 protocol=1"
#			else:
#				value = SOURCE_GCONF[value]
			value = SOURCE_GCONF[value]
			self.server.set_source_element(value)
	
	def _on_pipeline_change(self, value):
		print 'Got GConf pipeline change', value
		if value != None and value.type == gconf.VALUE_INT:
			value = PIPELINE_GCONF[value.get_int()]
			self.server.set_compression_pipeline(value)
		
if __name__ == "__main__":
	Streamer()
	reactor.run()
