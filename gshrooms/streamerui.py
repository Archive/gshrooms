# Gtk stuff
import pygtk
pygtk.require("2.0")

import gtk, gtk.glade,gobject,gconf

# i18n
from gettext import gettext as _
	
# Our modules
from constants import get_data_path
from settings import PIPELINE_LOW, PIPELINE_MED, PIPELINE_HIGH
from settings import SOURCE_SOUNDCARD, SOURCE_PLAYER, SOURCE_SINE

class StreamerUI:

	def __init__(self):
#		#Store the status as a dict
#		self.status = {}
#		
		# Setup glade		
		self.glade = gtk.glade.XML(get_data_path('gshrooms.glade'), root="streamer")
		self.window = self.glade.get_widget("streamer")		
		self.source_combo = self.glade.get_widget("source")
		self.pipeline_combo = self.glade.get_widget("pipeline")		
		self.close = self.glade.get_widget("close")

		# Get the gconf client
		self.client = gconf.client_get_default()
		
#		if has_sexy:
#			self.label = sexy.UrlLabel(_("The server is stopped."))
#		else:
#			self.label = gtk.Label(_("The server is stopped."))
#
#		self.glade.get_widget("cont").attach(self.label, 2, 3, 2, 3)
#		self.label.show()
#		
#		#Listen for now playing dbus events
#		self.nowplaying = NowPlaying()
		
		#Build profile combo box details
		store = gtk.ListStore(gobject.TYPE_STRING, gobject.TYPE_INT)
		store.append([_("Low Bandwidth Ogg/Vorbis"),    PIPELINE_LOW])
		store.append([_("Medium Bandwidth Ogg/Vorbis"), PIPELINE_MED])
		store.append([_("High Bandwidth Ogg/Vorbis"),   PIPELINE_HIGH])
		self.pipeline_combo.set_model(store)
		
		pipe = self.client.get("/apps/gshrooms/pipeline").get_int()	
		self.pipeline_combo.set_active(pipe)
		self.pipeline_combo.connect("changed", lambda x: self.client.set_int("/apps/gshrooms/pipeline", x.get_model()[x.get_active()][1]))
		
		#Build source combo box details				
		store = gtk.ListStore(gobject.TYPE_STRING, gobject.TYPE_INT)
		store.append([_("Soundcard Input"), SOURCE_SOUNDCARD])
		store.append([_("Rhythmbox"),       SOURCE_PLAYER])
		store.append([_("Test Source"),     SOURCE_SINE])
		self.source_combo.set_model(store)
		
		source = self.client.get("/apps/gshrooms/source").get_int()
		self.source_combo.set_active(source)
		self.source_combo.connect("changed", lambda x: self.client.set_int("/apps/gshrooms/source", x.get_model()[x.get_active()][1]))
		
		#Connect buttons
		self.close.connect("clicked", self._on_close)
		self.window.connect("destroy", self._on_close)
		
		#The icon
		self.window.set_icon(gtk.gdk.pixbuf_new_from_file(get_data_path('gshrooms.png')))
	def _on_close(self, button):
		gtk.main_quit()
	
#		# Server url
#		if has_sexy:
#			self.label.connect("url-activated", self._on_url_clicked)
#		
#		# Metadata from dbus	
#		self.nowplaying.connect('now-playing', self._on_update_now_playing)
#		self.nowplaying.connect('new-media', self._on_update_now_playing)
#			
#	# Private Methods
#	def _update_status(self):
#		artist, title, album = self.infos["meta"]
#		if artist == None or title == None or album == None:
#			metastring = ""
#		else:
#			metastring = _("<b>%s</b>: %s on %s") % (artist, title, album)
#		
#		
#		self.label.set_markup(statusstring+"\n"+metastring)
#		
#	def _on_update_now_playing(self, nowplaying):
#		infos = nowplaying.get_infos()
#		try:
#			self.infos["meta"] = (infos["artist"], infos["album"], infos["title"])
#		except:
#			self.infos["meta"] = (None, None, None)
#	
#	def _on_url_clicked(self, label, url):
#		os.spawnlp(os.P_NOWAIT, 'gnome-open', 'gnome-open', url)
#	
#	def _update_server_status(self):
#		#The server comes up
#		if self.active:
#			#We don't have info on IP yet
#			if self.net_info == None:
#				self.label.set_text(_("The server is active, opening ports..."));
#			#We have a net info, output the full label
#			else:
#				if isinstance(self.net_info, failure.Failure):
#					print self.net_info
#					self.label.set_markup(_("Error: <b>%s</b>") % self.net_info.value)
#				else:
#					accessible, ip, port = self.net_info
#					if not accessible:
#						if has_sexy:
#							self.label.set_markup(_("The server is active on local network only: <a href='http://%s:%s'>http://%s:%s</a>") % (ip, port, ip, port))
#						else:
#							self.label.set_text(_("The server is active on local network only: http://%s:%s") % (ip, port))
#					else:
#						if has_sexy:
#							self.label.set_markup(_("The server is active: <a href='http://%s:%s'>http://%s:%s</a>") % (ip, port, ip, port))
#						else:
#							self.label.set_text(_("The server is active: http://%s:%s") % (ip, port))
#					
#			
#			#Get the vu-meter from the server pipeline
#			level = self.server.get_level_element()
#			if level != None:
#				level.connect("level", self._on_vu_level)
#			
#			self.tray_icon.set_from_file(get_data_path("gshrooms-active.png"))
#		#The server goes down
#		else:
#			self.tray_icon.set_from_file(get_data_path("gshrooms-inactive.png"))
#			self.label.set_text(_("The server is stopped"));
#			
if __name__ == "__main__":
	StreamerUI()
	gtk.main()
