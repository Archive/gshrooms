import gst
import gobject

from settings import STREAMER_RECONNECTION_TIMEOUT

class MultifdSinkStreamer(gobject.GObject):
	__gsignals__ = {
		# Notify when a client disconnects fom the streamer
		# Parameters: the streamer and the disconnected FD
		'client-removed': 	(gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE,
							(gobject.TYPE_INT, gobject.TYPE_INT)),
		# The sink starts streaming		
		'start-streaming':	(gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE,
							()),
		# The sink stops streaming
		'stop-streaming':	(gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE,
							())
	}
	
	def __init__(self):
		"""Build the sink, the pipeline is inactive, the sink is paused."""
		gobject.GObject.__init__(self)
		
		# Mime type of the flowing media type		
		self._caps = None
		
		# The gst.Pipeline currently active, or None if no pipeline active
		self._pipeline = None
		self._watch_id = 0
		
		# The source element to use for the pipeline (as string)
		self._source = None
		# The compression pipeline to use for the pipeline (as gst parsable string)
		self._compress_pipeline = None
		
		# The ever-used multifdsink
		self._sink = gst.element_factory_make("multifdsink", "multifd")
		self._sink.set_property("buffers-max", 500) 
		self._sink.set_property("buffers-soft-max", 250)
		self._sink.set_property("recover-policy", 3) # Resync client to most recent keyframe
		self._sink.set_property("sync-method", 1) #  Make the new client wait for the next keyframe
		self._sink.set_property("timeout", 20000000000) # timeout of 20 seconds
		# Listen for caps nego, to know when to flow is on
		self._sink.connect('deep-notify::caps', self._on_caps_changed)
		# Listen for clients exit, to disconnect them properly
		self._sink.connect('client-removed', self._on_client_removed)
		self._sink.connect('client-added', self._on_client_added)
		self._sink.set_state(gst.STATE_PAUSED)
		
	# Public methods
	def has_flow(self):
		"""Checks wether we have a flow going through."""
		return self._caps != None
	
	def add_client(self, fd):
		"""Adds a new client to the streamer."""
		print '[fd %5d] Adding new client to streamer' % fd
		gobject.GObject.emit(self._sink, 'add', fd)
	            
	def get_content_type(self):
		"""Returns the mime-type string of flowing stream,
			or None if no stream is flowing."""
		if self._caps:
			return self._caps[0].get_name()
		else:
			return None
	
	def set_compression_pipeline(self, compress_pipeline):
		"""Sets a new compression pipeline, the streamer is restarted
			(all connections are closed)."""
		print 'New compression pipeline:', compress_pipeline
		self._compress_pipeline = compress_pipeline	
		self._rebuild_pipeline()
		
	def set_source_element(self, source):
		"""Sets a new pipeline source element, the streamer is paused and
			started with new source, the connections are preserved."""
		print 'New source element:', source
		self._source = source
		self._rebuild_pipeline()
	
	def get_level_element(self):
		"""Returns the level gst element in the current pipeline or None if no pipeline."""
		if self._pipeline != None:
			return self._pipeline.get_by_name("level")
		else:
			return None
	
	def stop(self):
		"""Close the streamer."""
		print 'Shutting down multifdsink & co'
		gobject.GObject.emit(self._sink, 'clear')
		if self._pipeline != None:
			self._pipeline.set_state(gst.STATE_NULL)
	
	def set_metadata(self, metadata):
		print 'Multifdsinkstreamer should send a new metadata header'
		
	# Private methods
	def _on_caps_changed(self, element, pad, param):
		"""Sets the self._caps to the newly negotiated flow."""
		self._caps = pad.get_negotiated_caps()
		if self._caps != None:
			print 'Caps Negotiated: %s' % self._caps[0].get_name()
			gobject.GObject.emit(self, 'start-streaming')
	
	def _on_client_removed(self, sink, fd, reason):
		"""Emit a signal telling the client can safely be disconnected."""
		print '[fd %5d] _on_client_removed: %s' % (fd,reason)
		print '[fd %5d] _on_client_removed'
		gobject.GObject.emit(self, 'client-removed', fd, reason)
	
	def _on_client_added(self, sink, fd):
		"""A new client is effectively added to multifdsink."""
		print '[fd %5d] _on_client_added'
		
	def _rebuild_pipeline(self):
		"""Rebuild the gstreamer pipeline:
			- If either the self._source or self._compress_pipeline is None, do nothing
		"""
		# Incomplete pipeline, do nothing
		if self._source == None or self._compress_pipeline == None:
			return
		
		# Existing pipeline, pause it, remove the idle iteration
		if self._pipeline != None:
			# Remove the paused server
			self._pipeline.set_state(gst.STATE_PAUSED)
			self._pipeline.get_by_name("last").unlink(self._sink)
			self._pipeline.remove(self._sink)
			
			# Destroy the pipeline
			self._pipeline.set_state(gst.STATE_NULL)
			gobject.source_remove(self._watch_id)
			del self._pipeline
			
			# We stop streaming, to rebuild the pipeline
			gobject.GObject.emit(self, 'stop-streaming')
		
		gobject.GObject.emit(self._sink, 'clear')
			
		self._caps = None
		self._pipeline = None
		
		# Rebuild
		print 'Building Pipeline: %s name=source ! %s ! identity name=last' % (self._source, self._compress_pipeline)
		try:
			#self._pipeline = gst.parse_launch('%s name=source ! nowplayingwriter ! %s ! identity name=last' % (self._source, self._compress_pipeline))
			self._pipeline = gst.parse_launch('%s name=source ! %s ! identity name=last' % (self._source, self._compress_pipeline))

			self._pipeline.add(self._sink)
			self._pipeline.get_by_name("last").link(self._sink)
			
			source = self._pipeline.get_by_name("source")
			source.connect('eos', self._setup_reconnect)

			state = self._pipeline.set_state(gst.STATE_PLAYING)
			if state == gst.STATE_FAILURE:
				self._setup_reconnect(source)
				
			self._watch_id = gobject.idle_add(self._pipeline.iterate)
		except gobject.GError, msg:
			# If an error occurs during pipeline building, we set pipeline to None
			print 'Error: %s' % msg
			self._pipeline = None
			self._caps = None
			
			# Forward error
			raise

	def _setup_reconnect(self, source):
		print 'Got disconnected, scheduling a reconnection'
		gobject.GObject.emit(self._sink, 'clear')
		gobject.timeout_add(STREAMER_RECONNECTION_TIMEOUT, self._on_reconnect)
		
	def _on_reconnect(self):
		self._rebuild_pipeline()
		return False
		
gobject.type_register(MultifdSinkStreamer)
