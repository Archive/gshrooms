from twisted.internet import defer

from nattraverso.ipdiscover import get_external_ip
from nattraverso.portmapper import get_port_mapper

class NatOpenError(Exception):
	pass

class NatOpen:
	
	def __init__(self, server):
		self._server = server
		self._port_mapper = None
	
	def open_ports(self):
		"""Detect if the user is accessible via internet
		-Direct connection or NAT with UPnP: return (True, external ip, external port)
		-localhost only, NAT but no UPnP: return (False, local ip, local port)."""
		result = defer.DeferredList([get_external_ip(), get_port_mapper()])			
		return result.addCallback(self._on_net_infos)
	
	def close_ports(self):
		if self._port_mapper != None:
			return defer.DeferredList([self._port_mapper.unmap(port) for port in self._server.get_ports()])
		else:
			return defer.succeed("No mapper found")
			
	def _on_net_infos(self, infos):
		(has_ip, ip_result), (has_port_mapper, port_mapper) = infos
		# We only support one port mapping right now, for the easy cases
		port = self._server.get_ports()[0]
				
		if has_ip:
			# We managed to retreive the ip address
			wan, ip = ip_result
			if wan == None or not wan:
				# We have a local address only, do no bother with port mapping
				return (False, ip, port.getHost().port)
		else:
			# Could not retreive the ip address, this is unrecoverable
			raise NatOpenError("No network available")
		
		#We have a WAN address, try to map the port if we can
		if not has_port_mapper:
			return (True, ip, port.getHost().port)
		else:
			self._port_mapper = port_mapper
			result = defer.DeferredList([port_mapper.map(port) for port in self._server.get_ports()])		
			return result.addCallback(lambda x:self._on_ports_mapped(x, ip))
	
	def _on_ports_mapped(self, mappings, ip):
		print 'ports mapped: %r @ %r' % (mappings, ip)
		# For now we support only one port mapping
		success, mapping = mappings[0]
		if not success:
			raise NatOpenError("Failed to map ports")
		else:
			extip, extport = mapping
			return (True, extip, extport)
