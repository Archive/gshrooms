import fcntl,os

from twisted.internet import reactor
from twisted.web import http, server, resource as web_resource

import gobject

from multifdsinkstreamer import MultifdSinkStreamer

ERROR_TEMPLATE = """
<!doctype html public "-//IETF//DTD HTML 2.0//EN">
<html>
	<head>
		<title>%(code)d %(error)s</title>
	</head>
	<body>
		<h1>Error</h1>
		<p>%(code)d %(error)s</p>
	</body>
</html>
"""
HTTP_SERVER = 'GnomeStreamer/v0.1'

class HttpStreamingServer(web_resource.Resource, gobject.GObject):
	
	# We have no child resources
	isLeaf = True
	
	__gsignals__ = {
		# The server starts streaming		
		'start-streaming':	(gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE,
							()),
		# The server stops streaming
		'stop-streaming':	(gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE,
							())
	}
	
	def __init__(self, maxclients=3):
		"""Build a new Streaming resource with the given MultifdSinkStreamer."""
		web_resource.Resource.__init__(self)
		gobject.GObject.__init__(self)

		self._streamer = MultifdSinkStreamer()
		self._streamer.connect('client-removed', self._on_client_removed)
		self._streamer.connect("start-streaming", self._on_start_streaming)
		self._streamer.connect("stop-streaming", self._on_stop_streaming)
		
		self._streamer_active = False
		
		self._requests = {}
		self._maxclients = maxclients
		
		#Ok, now build the http server
		self._ports = [reactor.listenTCP(10333, server.Site(resource=self))]
	
	#Public interface -------------------
	def set_max_clients(self, number):
		self._maxclients = number
		
	def set_source_element(self, source):
		self._streamer.set_source_element(source)
	
	def set_compression_pipeline(self, pipeline):
		self._streamer.set_compression_pipeline(pipeline)
		
	def get_ports(self):
		return self._ports
		
	def set_metadata(self, metadata):
		self._streamer.set_metadata(metadata)
		
	def stop(self):
		print 'Shutting down http server'
		self._streamer.stop()
	
	#Private Methods -------------------------------------------
	def _on_start_streaming(self, sink):
		self._streamer_active = True
			
	def _on_stop_streaming(self, sink):
		self._streamer_active = False
		
	# Private HTTP Methods ----------------------------------------------------
	def _check_valid_request(self, request):
		"""Checks wether we can accept this request.
		
		If the request is unacceptable, prepare the error doc, and return False,
		If it can be processed, return True.
		
		The request is finished if False is returned.
		"""
		if not self._can_accept_client() or not self._streamer.has_flow():
		    self._service_unavailable(request)
		    request.finish()
		    return False
			
		return True

	def _can_accept_client(self):
		"""Check wether a new client can be accepted in the streamer."""
		return len(self._requests) < self._maxclients
		
	def _service_unavailable(self, request):
		"""Write a UNAVAILABLE HTTP Code and write the error template html."""
		request.setHeader('content-type', 'text/html')
		request.setResponseCode(http.SERVICE_UNAVAILABLE)

		request.write(ERROR_TEMPLATE % {'code' : http.SERVICE_UNAVAILABLE,
										'error': http.RESPONSES[http.SERVICE_UNAVAILABLE]})

	def _write_headers(self, request):
		"""Write the headers to the request (mime, cache, etc)."""
		headers = []

		def set_header(field, name):
		    headers.append('%s: %s\r\n' % (field, name))
		    
		set_header('Server', HTTP_SERVER)
		set_header('Date', http.datetimeToString())
		set_header('Cache-Control', 'no-cache')
		set_header('Cache-Control', 'private')
		set_header('Content-type', self._streamer.get_content_type())
		
		try:
			os.write(request.transport.fileno(), 'HTTP/1.0 200 OK\r\n%s\r\n' % ''.join(headers))
			request.startedWriting = True
		except:
			pass
		
	def _handle_new_client(self, request):
		"""Write headers and start streaming to the client, return value is the return value of render_GET."""
		self._write_headers(request)

		# check if it's really an open fd
		fd = request.transport.fileno()
		try:
			fcntl.fcntl(fd, fcntl.F_GETFL)
		except IOError:
			request.finish()
			return

		# remove from twisted the FD
		reactor.removeReader(request.transport)

		# hand it to multifdsink
		self._streamer.add_client(fd)
		#Remember the corresponding request
		self._requests[fd] = request
		
		if len(self._requests) == 1:
			gobject.GObject.emit(self, 'start-streaming')
		
		return server.NOT_DONE_YET

	def _on_client_removed(self, streamer, fd, reason):
		"""When the streamer looses a client, we are notified and we close the connection."""
		try:
		    request = self._requests[fd]
		    request.transport.loseConnection()
		    del self._requests[fd]
		    
		    if len(self._requests) == 0:
				gobject.GObject.emit(self, 'stop-streaming')
				
		except KeyError:
		    print('[fd %5d] No such active FD' % fd)
	
	def render_GET(self, request):
		"""Renders the stream as http resource, and start streaming to the client."""
		print('[fd %5d] Incoming client connection from %s' % (request.transport.fileno(), request.getClientIP()))
		print('[fd %5d] render_GET(): request %s' % (request.transport.fileno(), request))
				
		# Invalid request, we stop it and close the connection
		if not self._check_valid_request(request):
			return
		
		print('[fd %5d] Accepting client' % (request.transport.fileno()))
		return self._handle_new_client(request)
		
	def render_HEAD(self, request):
		"""Renders the stream headers and close the connection."""
		print('[fd %5d] Incoming client connection from %s' % (request.transport.fileno(), request.getClientIP()))
		print('[fd %5d] render_HEAD(): request %s' % (request.transport.fileno(), request))
		
		self._write_headers(request)
		request.finish()
		
gobject.type_register(HttpStreamingServer)
