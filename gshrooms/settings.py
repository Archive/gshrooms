PIPELINE_LOW  = 0
PIPELINE_MED  = 1
PIPELINE_HIGH = 2

SOURCE_SOUNDCARD = 0
SOURCE_PLAYER    = 1
SOURCE_SINE      = 2

# Note: Keep this list and the enums in sync !
PIPELINE_GCONF = [
	"audioconvert ! audioscale ! audio/x-raw-int,rate=11025,channels=1 ! audioconvert ! rawvorbisenc bitrate=12000 max-bitrate=24000 ! oggmux",
	"audioconvert ! audioscale ! audio/x-raw-int,rate=22025,channels=1 ! audioconvert ! rawvorbisenc bitrate=24000 max-bitrate=32000 ! oggmux",
	"audioconvert ! audioscale ! audio/x-raw-int,rate=22025            ! audioconvert ! rawvorbisenc bitrate=32000 max-bitrate=64000 ! oggmux",
]
# Note: Keep this list and the enums in sync !
SOURCE_GCONF = [
	"alsasrc",
#FIXME: Don't use these props until gsteamer-0.9: "tcpclientsrc address=/tmp/rb-buddymusic socket-type=1 protocol=1",
	"tcpclientsrc host=localhost port=10334 protocol=1",
	"sinesrc",
]

STREAMER_SHUTDOWN_TIMEOUT = 30000 # 30 seconds
STREAMER_RECONNECTION_TIMEOUT = 10000 # 10 seconds

NOWPLAYING_DBUS_IFACE= 'org.gnome.NowPlaying'
NOWPLAYING_DBUS_PATH = '/org/gnome/NowPlaying'

GSHROOMS_DBUS_IFACE='org.gnome.gShrooms'
GSHROOMS_DBUS_PATH ='/org/gnome/gShrooms'

RHYTHMBOX_DBUS_IFACE='org.gnome.Rhythmbox.BuddyMusic'
RHYTHMBOX_DBUS_PATH ='/org/gnome/Rhythmbox/BuddyMusic'
