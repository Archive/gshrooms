import dbus
import dbus.service
if getattr(dbus, 'version', (0,0,0)) >= (0,41,0):
	import dbus.glib

from settings import GSHROOMS_DBUS_IFACE, GSHROOMS_DBUS_PATH

class RemoteControl(dbus.service.Object):
	def __init__(self, controller):
		bus_name = dbus.service.BusName(GSHROOMS_DBUS_IFACE, bus=dbus.SessionBus())
		dbus.service.Object.__init__(self, bus_name, GSHROOMS_DBUS_PATH)
		self.controller = controller
		
	@dbus.service.method(GSHROOMS_DBUS_IFACE)
	def GetUrl(self):
		accessible, ip, port = self.controller.get_net_info()
		if accessible:
			return "http://%s:%s/" % (ip, port)
		else:
			return ""
	
	@dbus.service.method(GSHROOMS_DBUS_IFACE)
	def GetSocketName(self):
		return self.controller.get_socket_name()
		
	@dbus.service.method(GSHROOMS_DBUS_IFACE)
	def KeepAlive(self):
		self.controller.keep_alive()
		
	@dbus.service.method(GSHROOMS_DBUS_IFACE)
	def Shutdown(self):
		self.controller.shutdown()
	
	@dbus.service.method(GSHROOMS_DBUS_IFACE)
	def AskShutdown(self):
		self.controller.ask_shutdown()

#	@dbus.service.signal('org.gnome.GnomeStreamer')
#	def hello_signal(self, message):
#		pass

def shutdown_existing():
	try:
		print 'Shutting down gshrooms'
		bus = dbus.SessionBus()
		gshrooms_obj = bus.get_object(GSHROOMS_DBUS_IFACE, GSHROOMS_DBUS_PATH)
		gshrooms_proxy = dbus.Interface(gshrooms_obj, GSHROOMS_DBUS_IFACE)
		gshrooms_proxy.Shutdown()
		print 'Running gShrooms has been shut down'
	except dbus.exceptions.DBusException, msg:
		print 'Warning:', msg
	
	from twisted.internet import reactor
	reactor.stop()
